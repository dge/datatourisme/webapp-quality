<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220405144431 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE "user" SET role = \'ROLE_MANAGER\' WHERE role = \'ROLE_PRODUCER\'');

    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE "user" SET role = \'ROLE_PRODUCER\' WHERE role = \'ROLE_MANAGER\'');
    }
}
