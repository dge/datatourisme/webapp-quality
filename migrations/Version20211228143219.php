<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211228143219 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getDescription(): string
    {
        return 'Initial data load';
    }

    public function up(Schema $schema): void
    {
        $this->setupOrganizationTypes();
        $this->setupProducers();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * Organization types
     *
     * @return void
     */
    private function setupOrganizationTypes()
    {

        // organisation types
        $types = [
            ['id' => 1, 'name' => 'Réseau départemental'],  // [1,2] Agences de développement touristique, Comité départemental de tourisme
            ['id' => 3, 'name' => 'Réseau régional'],       // [3] Comité régional de tourisme
            ['id' => 4, 'name' => 'Réseau communal ou intercommunal'], // [4] Office de tourisme
            ['id' => 5, 'name' => 'Autre type de réseau'],
        ];

        foreach ($types as $type) {
            $this->addSql('INSERT INTO organization_type (id, name) VALUES (:id, :name)', $type);
        }
    }

    private function setupProducers()
    {
        $producers = [
            ["id" => 1, 'name' => "CONJECTO", 'type_id' => 1],
            ["id" => 2, 'name' => "A SUPPRIMER - Tourisme 64", 'type_id' => 1],
            ["id" => 3, 'name' => "CDT Aube", 'type_id' => 1],
            ["id" => 4, 'name' => "A SUPPRIMER - SITLOR - SIT Lorraine", 'type_id' => 3],
            ["id" => 5, 'name' => "Alsace Destination Tourisme", 'type_id' => 1],
            ["id" => 6, 'name' => "CRT Bourgogne Franche Comté - Décibelles Data", 'type_id' => 3],
            ["id" => 7, 'name' => "Bouches du Rhône Tourisme", 'type_id' => 1],
            ["id" => 8, 'name' => "DGE - Direction Générale des Entreprises", 'type_id' => 3],
            ["id" => 9, 'name' => "TEST - TOURISME & TERRITOIRES", 'type_id' => 1],
            ["id" => 10, 'name' => "Agence Régionale du Tourisme Grand Est", 'type_id' => 3],
            ["id" => 11, 'name' => "DATAtourisme COPIL", 'type_id' => 1],
            ["id" => 13, 'name' => "Apidae Tourisme", 'type_id' => 3],
            ["id" => 14, 'name' => "Pas de Calais Tourisme", 'type_id' => 1],
            ["id" => 15, 'name' => "CRT CENTRE - VAL DE LOIRE", 'type_id' => 3],
            ["id" => 17, 'name' => "A SUPPRIMER - TEST BRETAGNE", 'type_id' => 1],
            ["id" => 19, 'name' => "Office du Tourisme et des Congrès de Paris", 'type_id' => 4],
            ["id" => 20, 'name' => "Charentes Tourisme", 'type_id' => 1],
            ["id" => 21, 'name' => "SIT Limousin", 'type_id' => 1],
            ["id" => 22, 'name' => "Lot Tourisme", 'type_id' => 1],
            ["id" => 23, 'name' => "CRT Nouvelle Aquitaine", 'type_id' => 3],
            ["id" => 24, 'name' => "A SUPPRIMER - CRT Bourgogne Franche Comté", 'type_id' => 3],
            ["id" => 26, 'name' => "ADT de la Marne", 'type_id' => 1],
            ["id" => 28, 'name' => "SIT PICARDIE", 'type_id' => 3],
            ["id" => 29, 'name' => "Nord Tourisme", 'type_id' => 1],
            ["id" => 30, 'name' => "Normandie Tourisme", 'type_id' => 3],
            ["id" => 31, 'name' => "ADT de l'Aveyron", 'type_id' => 1],
            ["id" => 32, 'name' => "Hérault Tourisme", 'type_id' => 1],
            ["id" => 33, 'name' => "CDT Haute Garonne", 'type_id' => 1],
            ["id" => 34, 'name' => "DEPARTEMENT DEUX SEVRES", 'type_id' => 1],
            ["id" => 35, 'name' => "Comité Martiniquais du Tourisme", 'type_id' => 1],
            ["id" => 36, 'name' => "Hautes-Pyrénées Tourisme Environnement", 'type_id' => 1],
            ["id" => 37, 'name' => "Agence Touristique de la Vienne", 'type_id' => 1],
            ["id" => 38, 'name' => "SIT Bretagne", 'type_id' => 3],
            ["id" => 39, 'name' => "ADT Ariège Pyrénées", 'type_id' => 1],
            ["id" => 41, 'name' => "ADT Aude", 'type_id' => 1],
            ["id" => 42, 'name' => "ADT Ardennes", 'type_id' => 1],
            ["id" => 43, 'name' => "Comité Départemental du Tourisme Destination Gers", 'type_id' => 1],
            ["id" => 44, 'name' => "SEINE SAINT DENIS TOURISME", 'type_id' => 1],
            ["id" => 45, 'name' => "CDT Lozère", 'type_id' => 1],
            ["id" => 46, 'name' => "Morbihan Tourisme", 'type_id' => 1],
            ["id" => 47, 'name' => "Gard Tourisme", 'type_id' => 1],
            ["id" => 48, 'name' => "Maison Départementale du Tourisme de Haute Marne", 'type_id' => 1],
            ["id" => 49, 'name' => "SIT Pays de la Loire", 'type_id' => 3],
            ["id" => 50, 'name' => "Tarn-et-Garonne Tourisme", 'type_id' => 1],
            ["id" => 51, 'name' => "CDT Essonne", 'type_id' => 1],
            ["id" => 52, 'name' => "Comité Départemental du Tourisme du Jura", 'type_id' => 1],
            ["id" => 53, 'name' => "SIT Guadeloupe", 'type_id' => 3],
            ["id" => 54, 'name' => "Région Réunion", 'type_id' => 1],
            ["id" => 55, 'name' => "Agence du Tourisme de la Corse", 'type_id' => 3],
            ["id" => 56, 'name' => "Pyrénées Orientales Tourisme", 'type_id' => 1],
            ["id" => 57, 'name' => "Vendée Expansion", 'type_id' => 1],
            ["id" => 58, 'name' => "Provence Tourisme", 'type_id' => 1]
        ];

        foreach ($producers as $producer) {
            $this->addSql('INSERT INTO producer (id, name, type_id) VALUES (:id, :name, :type_id)', $producer);
        }
    }
}
