<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220325092717 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "user" ADD periodic_notifications TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD threshold_notifications JSON DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN "user".periodic_notifications IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE "user" DROP periodic_notifications');
        $this->addSql('ALTER TABLE "user" DROP threshold_notifications');
    }
}
