/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import moment from "moment";
import ReactDOM from "react-dom";
import Dashboard from "./Dashboard";
import "./styles.scss";

moment.locale('fr');

const element = document.getElementById("react-root");
ReactDOM.render(<Dashboard url={element.dataset.url}></Dashboard>, element);
