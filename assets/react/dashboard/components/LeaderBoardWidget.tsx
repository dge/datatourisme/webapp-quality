/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useContext, useEffect, useState } from "react";
import { DashboardContext } from "../Dashboard";
import { ResponsiveBar } from "@nivo/bar";
import fontColorContrast from "font-color-contrast";
import { format } from "../../utils/number";

const BarComponent = ({ bar /*, borderColor*/ }: any) => {
  return (
    <g transform={`translate(${bar.x},${bar.y})`}>
      {/* <rect x={0} y={2} width={bar.width + 2} height={bar.height / 2} fill="rgba(0, 0, 0, .07)" /> */}
      <rect width={bar.width} height={bar.height / 2} fill={bar.color} />
      <text
        x={5}
        y={(bar.height * 0.5) + (11/2)}
        textAnchor="start"
        dominantBaseline="central"
        fill="black"
        style={{
          fontWeight: 400,
          fontSize: 11,
        }}
      >
        {bar.data.indexValue}
      </text>
      <text
        x={bar.width - 4}
        y={bar.height / 4 - 1}
        textAnchor="end"
        dominantBaseline="central"
        fill={fontColorContrast(bar.color, 0.5)}
        style={{
          fontWeight: 800,
          fontSize: 13,
        }}
      >
        {format(bar.data.value)}
      </text>
    </g>
  );
};

type Mode = {
  field: string;
};

const MODES: { [key: string]: Mode } = {
  sit: { field: "hasOrganizationIdentifier" },
  regions: { field: "isLocatedAt.address.hasAddressCity.isPartOfDepartment.isPartOfRegion.label.@fr.raw" },
  departments: { field: "isLocatedAt.address.hasAddressCity.isPartOfDepartment.label.@fr.raw" },
  cities: { field: "isLocatedAt.address.hasAddressCity.label.@fr.raw" },
  creators: { field: "hasBeenCreatedBy.legalName" },
};

const LeaderBoardWidget = () => {
  const { client, producers } = useContext(DashboardContext);
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<any[]>([]);
  const [selectedMode, setSelectedMode] = useState<string>("sit");

  const mode = MODES[selectedMode];

  const processBuckets = (data) => {
    return data
      .map((b: any) => {
        const id = selectedMode == "sit" ? producers[b.key] : b.key;
        return { id, value: b.doc_count };
      })
      .sort((a, b) => a.value - b.value);
  };

  useEffect(() => {
    setLoading(true);
    client
      .search(
        "datatourisme",
        {
          size: 0,
          body: {
            query: { match_all: {} },
            aggs: {
              agg: {
                terms: {
                  size: 10,
                  field: mode.field,
                },
              },
            },
          },
        },
        { params: { all: 1 } }
      )
      .then((data) => {
        setData(processBuckets(data.aggregations.agg.buckets));
        setLoading(false);
      });
  }, [mode]);

  // @see https://coolors.co/gradient-palette
  // @see https://pinetools.com/lighten-color
  // const colors = ["#CCFBF1","#BFEFE6","#B3E3DA","#A6D7CF","#99CBC4","#8DC0B8","#80B4AD","#73A8A2","#679C96","#5A908B"];
  // const colors = ["#E0F2FE", "#CAE7F9", "#B4DCF3", "#9ED1EE", "#88C6E8", "#73BCE3", "#5DB1DD", "#47A6D8", "#319BD2", "#1B90CD"];
  const colors = [
    "#0284C7",
    "#037EBD",
    "#0477B3",
    "#0571A9",
    "#066A9F",
    "#086496",
    "#095D8C",
    "#0A5782",
    "#0B5078",
    "#0C4A6E",
  ];

  return (
    <div className={`widget ${loading ? "loading" : ""}`}>
      <div className="box box-default mb-0 drop-shadow-md">
        <div className="box-header with-border">
          <h3 className="box-title">Classement général</h3>
          <div className="box-tools pull-right">
            <select
              value={selectedMode}
              onChange={(e) => setSelectedMode(e.currentTarget.value)}
              className="form-control input-xs h-6 py-0 px-1 text-xs"
            >
              <option value="sit">Top 10 : SIT</option>
              <option value="regions">Top 10 : Régions</option>
              <option value="departments"> Top 10 : Départements</option>
              <option value="cities"> Top 10 : Communes</option>
              <option value="creators">Top 10 : Créateurs</option>
            </select>
          </div>
        </div>
        <div className="box-body p-0 h-[280px]">
          <ResponsiveBar
            layout="horizontal"
            animate={false}
            margin={{ top: 0, right: 20, bottom: 0, left: 0 }}
            data={data}
            indexBy="id"
            keys={["value"]}
            colors={colors}
            colorBy="indexValue"
            borderColor={{ from: "color", modifiers: [["darker", 2.6]] }}
            enableGridX
            enableGridY={false}
            // axisTop={{
            //   format: "~s",
            // }}
            // axisBottom={{
            //   format: "~s",
            // }}
            axisTop={null}
            axisBottom={null}
            axisLeft={null}
            padding={0}
            labelTextColor={{ from: "color", modifiers: [["darker", 1.4]] }}
            isInteractive={false}
            barComponent={BarComponent}
          />
        </div>
      </div>
    </div>
  );
};

export default LeaderBoardWidget;
