/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useContext, useEffect, useState } from "react";
import { format } from "../../utils/number";
import { DashboardContext } from "../Dashboard";

import { ResponsivePie } from "@nivo/pie";
import { SearchTotalHits } from "../../utils/es-client";
import { WidgetInnerProps } from "../../interfaces";
import { types } from "../../variables";

export const POIRepartitionWidgetInner = (props: WidgetInnerProps) => {
  const { setLoading } = props;
  const { client, theme, colors } = useContext(DashboardContext);
  const [rawData, setRawData] = useState<any>();

  const [selectedKey, setSelectedKey] = useState<string | undefined>(undefined);
  const [objectCount, setObjectCount] = useState<number | null>(null);
  const [data, setData] = useState<any[]>([]);

  useEffect(() => {
    setLoading(true);
    client
      .search("datatourisme", {
        size: 0,
        track_total_hits: true,
        body: {
          query: { match_all: {} },
          aggs: {
            type: {
              terms: {
                size: 10,
                field: "analyze.type1",
              },
              aggs: {
                type: {
                  terms: {
                    size: 10,
                    field: "analyze.type2",
                  },
                },
              },
            },
          },
        },
      })
      .then((data) => {
        setRawData(data);
        setLoading(false);
      });
  }, []);

  const processBuckets = (data) => {
    return data.map((b: any) => {
      return {
        id: b.key,
        value: b.doc_count,
        label: `${b.key.substr(0, 25)}${b.key.length > 25 ? "…" : ""}`, // for limit legend size
      };
    });
  };

  useEffect(() => {
    if (rawData) {
      if (selectedKey) {
        const bucket = rawData.aggregations.type.buckets.filter((b: any) => b.key == selectedKey)[0];
        setObjectCount(bucket.doc_count);
        setData(processBuckets(bucket.type.buckets));
      } else {
        setObjectCount((rawData.hits.total as SearchTotalHits).value);
        setData(processBuckets(rawData.aggregations.type.buckets));
      }
    }
  }, [rawData, selectedKey]);

  const total = data.reduce((p, c, i) => {
    return p + c.value;
  }, 0);

  /**
   * Metric layer
   */
  const CenteredMetric = ({ centerX, centerY }: any) => {
    return (
      objectCount && (
        <text
          x={centerX}
          y={centerY}
          textAnchor="middle"
          dominantBaseline="central"
          style={{
            fontSize: "30px",
            fontWeight: 700,
            color: "#333",
          }}
        >
          {format(objectCount)}
        </text>
      )
    );
  };

  const onClick = (datum, e) => {
    window.location.href = `/explorer?${selectedKey ? "type2" : "type"}=["${datum.id}"]`;
  };

  return (
    <div className="relative h-full">
      <select
        value={selectedKey}
        onChange={(e) => setSelectedKey(e.currentTarget.value || undefined)}
        className="form-control input-xs h-6 py-0 px-1 text-xs w-auto absolute top-[8px] right-[20px] z-10"
      >
        <option value="">Tous</option>
        {rawData?.aggregations.type.buckets &&
          rawData.aggregations.type.buckets.map((b: any) => (
            <option value={b.key} key={b.key}>
              {b.key}
            </option>
          ))}
      </select>
      <ResponsivePie
        margin={{ top: 20, right: 20, bottom: 20, left: -140 }}
        data={data}
        animate={true}
        activeOuterRadiusOffset={5}
        innerRadius={0.7}
        colors={colors}
        padAngle={1}
        startAngle={75}
        endAngle={435}
        valueFormat={format}
        enableArcLabels={true}
        enableArcLinkLabels={false}
        arcLabel={(d) => `${Math.round((d.value * 100) / total)}%`}
        arcLabelsTextColor="#FFFFFF"
        arcLabelsSkipAngle={10}
        onClick={onClick}
        onMouseEnter={(z, e) => {
          e.currentTarget.style.cursor = "pointer";
        }}
        activeInnerRadiusOffset={5}
        theme={{ ...theme, labels: { text: { fontWeight: 600, fontSize: "13px" } } }}
        layers={["arcs", "arcLabels", "arcLinkLabels", "legends", CenteredMetric]}
        legends={[
          {
            anchor: "bottom-right",
            direction: "column",
            justify: false,
            itemsSpacing: 5,
            itemWidth: 150,
            itemHeight: 18,
            itemDirection: "right-to-left",
            itemOpacity: 1,
            symbolSize: 18,
            symbolShape: "square",
            onClick,
          },
        ]}
      />
    </div>
  );
};

const POIRepartitionWidget = () => {
  const [loading, setLoading] = useState(true);

  return (
    <div className={`widget ${loading ? "loading" : ""}`}>
      <div className="box box-default mb-0 drop-shadow-md">
        <div className="box-header with-border">
          <h3 className="box-title">Répartition de vos POI</h3>
        </div>
        <div className="box-body p-0 h-[280px]">
          <POIRepartitionWidgetInner setLoading={setLoading} />
        </div>
      </div>
    </div>
  );
};

export default POIRepartitionWidget;
