/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useContext, useEffect, useState } from "react";
import { format } from "../../utils/number";
import { DashboardContext } from "../Dashboard";

import { ResponsivePie } from "@nivo/pie";
import { analyzers } from "../../variables";
import { WidgetInnerProps } from "../../interfaces";

export const AnomalyRepartitionWidgetInner = (props: WidgetInnerProps) => {
  const { setLoading } = props;
  const { client, theme } = useContext(DashboardContext);

  const [anomalyCount, setAnomalyCount] = useState(null);
  const [data, setData] = useState<any[]>([]);

  const processBuckets = (data) => {
    return data.map((b: any) => {
      const { key } = b;
      const { label, color } = analyzers[key];
      return { id: label || key, value: b.doc_count, color };
    });
  };

  useEffect(() => {
    setLoading(true);
    client
      .search("datatourisme", {
        size: 0,
        body: {
          query: { match_all: {} },
          aggs: {
            anomalies: {
              nested: {
                path: "analyze.anomalies",
              },
              aggs: {
                analyzer: {
                  terms: {
                    field: "analyze.anomalies.analyzer",
                  },
                },
              },
            },
          },
        },
      })
      .then((data) => {
        setAnomalyCount(data.aggregations.anomalies.doc_count);
        setData(processBuckets((data.aggregations.anomalies as any).analyzer.buckets));
        setLoading(false);
      });
  }, []);

  const total = data.reduce((p, c, i) => {
    return p + c.value;
  }, 0);

  const CenteredMetric = ({ dataWithArc, centerX, centerY }: any) => {
    return (
      anomalyCount && (
        <text
          x={centerX}
          y={centerY}
          textAnchor="middle"
          dominantBaseline="central"
          style={{
            fontSize: "30px",
            fontWeight: 700,
            color: "#333",
          }}
        >
          {format(anomalyCount)}
        </text>
      )
    );
  };

  const onClick = (datum, e) => {
    const anomalyAnalyzerKey = Object.keys(analyzers).find(key => analyzers[key]['label'] === datum.id);
    window.location.href = `/explorer?anomalyAnalyzer=["${anomalyAnalyzerKey}"]`;
  }

  return (
    <ResponsivePie
      margin={{ top: 20, right: -120, bottom: 20, left: 20 }}
      data={data}
      animate={true}
      colors={{ datum: "data.color" }}
      activeOuterRadiusOffset={5}
      innerRadius={0.7}
      padAngle={1}
      valueFormat={format}
      enableArcLinkLabels={false}
      enableArcLabels={true}
      arcLabel={(d) => `${Math.round((d.value * 100) / total)}%`}
      //arcLabelsTextColor={{ from: "color", modifiers: [['darker', 2]] }}
      arcLabelsTextColor="#FFFFFF"
      arcLabelsSkipAngle={10}
      // arcLinkLabel={arcLinkLabel}
      activeInnerRadiusOffset={5}
      onClick={onClick}
      onMouseEnter={(z,e) => {
        e.currentTarget.style.cursor = 'pointer';
      }}
      theme={{ ...theme, labels: { text: { fontWeight: 600, fontSize: "13px" } } }}
      layers={["arcs", "arcLabels", "arcLinkLabels", "legends", CenteredMetric]}
      legends={[
        {
          anchor: "bottom-left",
          direction: "column",
          justify: false,
          itemsSpacing: 5,
          itemWidth: 150,
          itemHeight: 18,
          itemDirection: "left-to-right",
          itemOpacity: 1,
          symbolSize: 18,
          symbolShape: "square",
          onClick
        },
      ]}
    />
  );
};

const AnomalyRepartitionWidget = () => {
  const [loading, setLoading] = useState(true);

  return (
    <div className={`widget ${loading ? "loading" : ""}`}>
      <div className="box box-default= drop-shadow-md">
        <div className="box-header with-border">
          <h3 className="box-title">Répartition de vos anomalies</h3>
        </div>
        <div className="box-body p-0 h-[280px]">
          <AnomalyRepartitionWidgetInner setLoading={setLoading} />
        </div>
      </div>
    </div>
  );
};

export default AnomalyRepartitionWidget;
