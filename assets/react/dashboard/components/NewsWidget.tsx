/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import useAxios from "axios-hooks";
import Moment from "react-moment";
import ReactMarkdown from 'react-markdown'

const NewsWidget = () => {
  const [{ data: news, loading, error }, refetch] = useAxios("/news/api/last");

  return (
    <div className={`widget h-full ${loading ? "loading" : ""}`}>
      <div className="box box-default mb-0 drop-shadow-md h-full flex flex-col">
        <div className="box-header with-border">
          <h3 className="box-title">
            {!news && "\u00A0"}
            {news && (
              <div className="flex items-center">
                <span className="px-2 leading-5 font-normal rounded-md bg-gray-100 text-gray-800 mr-2">
                  <Moment format="DD MMM" titleFormat="DD MMMM YYYY" withTitle>
                    {news.date}
                  </Moment>
                </span>
                {news.title}
              </div>
            )}
          </h3>
          <div className="box-tools pull-right">
            <a href="/news" className="btn btn-box-tool text-xs">
              <i className="fa fa-mail-forward"></i> Toutes les actualités
            </a>
          </div>
        </div>
        <div className="box-body flex-1 text-sm h-64 md:h-0 basis-auto overflow-auto">
          {news && <ReactMarkdown>{news.body}</ReactMarkdown>}
        </div>
      </div>
    </div>
  );
};

export default NewsWidget;
