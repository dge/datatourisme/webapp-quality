/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import useAxios from "axios-hooks";
import { useEffect, useRef } from "react";

import Map from 'ol/Map'
import { fromLonLat } from 'ol/proj'
import View from 'ol/View'
import TileLayer from 'ol/layer/Tile'
import HeatmapLayer from 'ol/layer/Heatmap';
import VectorSource from 'ol/source/Vector'
import ClusterSource from 'ol/source/Cluster'
// import WebGLPointsLayer from 'ol/layer/WebGLPoints';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import {defaults as defaultControls} from 'ol/control';
import Layer from "ol/layer/Layer";
import { getDefaultBaseLayer } from "../../utils/openlayers";

const createFeature = (item: any): Feature<Point> => {
  const { lon, lat, label } = item;
  return new Feature({ geometry: new Point(fromLonLat([lon,lat])) })
}

const MapWidget = () => {
  const [{ data, loading, error }, refetch] = useAxios("/dashboard/data/map");

  const mapElement = useRef<HTMLDivElement>(null);
  const map = useRef<Map>(null);
  const layer = useRef<Layer<any, any>>(null);

  const center = [1.71, 46.71];
  const zoom = 4;

  // initialize map on first render - logic formerly put into componentDidMount
  useEffect(() => {
    // create map
    const _map = new Map({
      target: mapElement.current!,
      layers: [getDefaultBaseLayer()],
      view: new View({
        projection: 'EPSG:3857',
        center: fromLonLat(center),
        zoom
      }),
      controls: defaultControls({
        zoom: true
      }),
    });

    // save map and vector layer references to state
    map.current = _map;
  }, []);

  // update data
  useEffect(() => {
    if (map.current && data) {
      const features = data.map((hit: any) => createFeature(hit));

      const source = new VectorSource({ features });
      const clusterSource = new ClusterSource({
        distance: 20,
        source
      });

      const _layer = new HeatmapLayer({
        source : clusterSource,
        weight: function(feature) { return feature.get('features').length; },
        radius: 18,
        blur: 36
      });

      if (layer.current) {
        map.current.removeLayer(layer.current);
      }

      map.current.addLayer(_layer);
      layer.current = _layer;

      map.current.getView().fit(source.getExtent());
    }
  }, [map, data]);

  return (
    <div className={`widget ${loading ? "loading" : ""}`}>
      <div className="box box-default mb-0 drop-shadow-md">
        <div className="box-header with-border">
          <h3 className="box-title">Carte de vos POI</h3>
        </div>
        <div className="box-body p-0 h-[280px]">
          <div ref={mapElement} className="map-container w-full h-full" />
        </div>
      </div>
    </div>
  );
};

export default MapWidget;
