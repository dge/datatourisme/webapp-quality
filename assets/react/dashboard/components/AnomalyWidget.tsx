/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useState } from "react";
import { Tab, TabPanel, Tabs } from "../../components/Tabs";
import { AnomalyEvolutionWidgetInner } from "./AnomalyEvolutionWidget";
import { AnomalyRepartitionWidgetInner } from "./AnomalyRepartitionWidget";

const AnomalyWidget = () => {
  const [loading, setLoading] = useState(true);
  const [view, setView] = useState("repartition");

  return (
    <div className={`widget ${loading ? "loading" : ""}`}>
      <div className="box box-default mb-0 drop-shadow-md">
        <Tabs selectedItem={view} onChange={(k) => setView(k)}>
          <div className="box-header with-border p-0">
            <div className="flex">
              <h3 className="box-title py-[10px] px-[15px]">Anomalies sur vos POI</h3>
              <div className="flex flex-wrap -mb-px list-none m-0 p-0 text-sm content-end">
                <Tab className="text-sm py-2 px-2" item="repartition">Répartition</Tab>
                <Tab className="text-sm py-2 px-2" item="evolution">Évolution</Tab>
              </div>
            </div>
          </div>
          <div className="box-body p-0 h-[280px]">
            <TabPanel item="repartition" children={<AnomalyRepartitionWidgetInner setLoading={setLoading} />} />
            <TabPanel item="evolution" children={<AnomalyEvolutionWidgetInner setLoading={setLoading} />} />
          </div>
        </Tabs>
      </div>
    </div>
  );
};

export default AnomalyWidget;
