/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import axios, { AxiosInstance, AxiosRequestConfig } from "axios";

export type DefaultOperator = "AND" | "OR";
export type Field = string;
export type Fields = Field | Field[];
export type ExpandWildcards = "open" | "closed" | "none" | "all";
export type Time = string | number;
export type SearchType = "query_then_fetch" | "dfs_query_then_fetch";
export type SuggestMode = "missing" | "popular" | "always";
export type VersionType = "internal" | "external" | "external_gte" | "force";

export interface SearchDocValueField {
  field: Field;
  format?: string;
}

export interface DateField {
  field: Field;
  format?: string;
  include_unmapped?: boolean;
}

export interface SearchSourceFilter {
  excludes?: Fields;
  includes?: Fields;
  exclude?: Fields;
  include?: Fields;
}

export interface SearchPointInTimeReference {
  id: string;
  keep_alive?: Time;
}

export interface SearchRequest {
  allow_no_indices?: boolean;
  allow_partial_search_results?: boolean;
  analyzer?: string;
  analyze_wildcard?: boolean;
  batched_reduce_size?: number;
  ccs_minimize_roundtrips?: boolean;
  default_operator?: DefaultOperator;
  df?: string;
  docvalue_fields?: Fields;
  expand_wildcards?: ExpandWildcards;
  explain?: boolean;
  ignore_throttled?: boolean;
  ignore_unavailable?: boolean;
  lenient?: boolean;
  max_concurrent_shard_requests?: number;
  min_compatible_shard_node?: string;
  preference?: string;
  pre_filter_shard_size?: number;
  request_cache?: boolean;
  routing?: string;
  scroll?: Time;
  search_type?: SearchType;
  stats?: string[];
  stored_fields?: Fields;
  suggest_field?: Field;
  suggest_mode?: SuggestMode;
  suggest_size?: number;
  suggest_text?: string;
  terminate_after?: number;
  timeout?: Time;
  track_total_hits?: boolean | number;
  track_scores?: boolean;
  typed_keys?: boolean;
  rest_total_hits_as_int?: boolean;
  version?: boolean;
  _source?: boolean | Fields;
  _source_excludes?: Fields;
  _source_includes?: Fields;
  seq_no_primary_term?: boolean;
  q?: string;
  size?: number;
  from?: number;
  sort?: string | string[];
  body?: {
    aggregations?: any;
    aggs?: any;
    from?: number;
    highlight?: any;
    track_total_hits?: boolean;
    indices_boost?: { [key: string]: number }[];
    docvalue_fields?: SearchDocValueField | (Field | SearchDocValueField)[];
    min_score?: number;
    post_filter?: any;
    profile?: boolean;
    query?: any;
    rescore?: any;
    script_fields?: any;
    search_after?: (number | string | null)[];
    size?: number;
    slice?: any;
    sort?: any;
    _source?: boolean | Fields | SearchSourceFilter;
    fields?: (Field | DateField)[];
    suggest?: any;
    terminate_after?: number;
    timeout?: string;
    track_scores?: boolean;
    version?: boolean;
    seq_no_primary_term?: boolean;
    stored_fields?: Fields;
    pit?: SearchPointInTimeReference;
    runtime_mappings?: any;
    stats?: string[];
  };
}

export interface ShardsResponse {
  total: number;
  successful: number;
  failed: number;
  skipped: number;
}

export interface Explanation {
  value: number;
  description: string;
  details: Explanation[];
}

export interface SearchTotalHits {
  relation: "eq" | "gte";
  value: number;
}

export interface SearchResponse<T = {[key: string]: any}> {
  took: number;
  timed_out: boolean;
  _scroll_id?: string | undefined;
  _shards: ShardsResponse;
  hits: {
    total: SearchTotalHits | number;
    max_score: number;
    hits: Array<{
      _index: string;
      _type: string;
      _id: string;
      _score: number;
      _source: T;
      _version?: number | undefined;
      _explanation?: Explanation | undefined;
      fields?: any;
      highlight?: any;
      inner_hits?: any;
      matched_queries?: string[] | undefined;
      sort?: string[] | undefined;
    }>;
  };
  aggregations?: any;
  suggest?: any[];
}

export interface GetRequest {
  stored_fields?: Fields | undefined;
  parent?: string | undefined;
  preference?: string | undefined;
  realtime?: boolean | undefined;
  refresh?: boolean | undefined;
  routing?: string | undefined;
  _source?: boolean | Fields;
  version?: number | undefined;
  version_type?: VersionType | undefined;
  id: string;
}

export interface GetResponse<T = {[key: string]: any}> {
  _index: string;
  _type: string;
  _id: string;
  _version: number;
  _routing?: string | undefined;
  found: boolean;
  _source: T;
}

/**
 * Client
 */
class ElasticsearchClient {
  axiosInstance: AxiosInstance;

  constructor(url: string, config?: AxiosRequestConfig) {
    this.axiosInstance = axios.create({ baseURL: url, ...config });
  }

  async search<T = {[key: string]: any}>(index: string, data: SearchRequest, config?: AxiosRequestConfig): Promise<SearchResponse<T>> {
    const { body, ...rest } = data;
    const _data = { ...body, ...rest };
    return this.axiosInstance
      .post(`${index}/_search`, _data, config)
      .then((data) => data.data);
  }

  async get<T = {[key: string]: any}>(index: string, data: GetRequest, config?: AxiosRequestConfig): Promise<GetResponse<T>> {
    const { id, ...params } = data;
    const encodedId = encodeURIComponent(encodeURIComponent(id)); // double encode is needed due to proxy
    return this.axiosInstance
      .get(`${index}/_doc/${encodedId}`, { ...config, params })
      .then((data) => data.data);
  }
}

export default ElasticsearchClient;
