/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

const extract = (obj: any, path: string, defaultValue?: any): any => {
  const parts = path.split(".");
  var len = parts.length;
  for (var i = 0; i < len; i++) {

    // todo : only for last step ?
    if (Array.isArray(obj)) {
      const p = parts.slice(i).join(".");
      return obj.map((o) => extract(o, p)).filter(v => v);
    }

    if (
      obj &&
      (Object.prototype.hasOwnProperty.call(obj, parts[i]) || obj[parts[i]])
    ) {
      obj = obj[parts[i]];
    } else {
      return defaultValue !== undefined ? defaultValue : null;
    }
  }
  return obj !== undefined ? obj : null;
};

export const extractAsArray = (obj: any, path: string): any => {
  const value = extract(obj, path, []);
  if (!Array.isArray(value)) {
    return [value];
  }

  return value;
};

export default extract;
