/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

export const format = (number: number, options?: Intl.NumberFormatOptions) => {
  return new Intl.NumberFormat('fr-FR', options).format(number);
}

