/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import {useMemo} from "react";
import {Column, FormatterProps, HeaderRendererProps} from "react-data-grid";
import {DndProvider} from "react-dnd";
import {HTML5Backend} from "react-dnd-html5-backend";
import Moment from "react-moment";
import "moment/locale/fr";

import extract from "../../utils/extract";
import ResultDataGrid, { ResultDataGridProps } from "../../components/ResultDataGrid";
import {DraggableHeaderRenderer} from "./header/DraggableHeaderRenderer";
import AnomalyStatsFormatter from "./formatter/AnomalyStatsFormatter";

import MediaStatsFormatter from "./formatter/MediaStatsFormatter";
import useLocalStorage from "../../hooks/useLocalStorage";

Moment.globalLocale = "fr";

type CustomResultDataGridProps = {
  onLabelClick: (row: { [s: string]: any }) => void;
} & Omit<ResultDataGridProps, "columns">;

const CustomResultDataGrid = (props: CustomResultDataGridProps) => {
  const { onLabelClick } = props;

  const columns = [
    {
      key: "label.@fr",
      sortKey: "label.@fr.raw",
      name: "Nom",
      frozen: true,
      width: 300,
      cellClass: "cursor-pointer",
    },
    {
      key: "identifier",
      name: "Identifiant",
      width: 150,
    },
    {
      key: "analyze.anomalyStatistics.total",
      name: "Erreurs",
      formatter: AnomalyStatsFormatter,
    },
    {
      key: "analyze.mediaCount",
      name: "Médias",
      formatter: MediaStatsFormatter,
    },
    {
      key: "lastUpdate",
      name: "Mise à jour",
      width: 150,
      formatter: ({row}: FormatterProps<any>) => <Moment format="D MMM YYYY">{row["lastUpdate"]}</Moment>,
    },
    {
      key: "lastUpdateDatatourisme",
      name: "Mise à jour DATAtourisme",
      formatter: ({row}: FormatterProps<any>) => <Moment format="D MMM YYYY">{row["lastUpdateDatatourisme"]}</Moment>,
      width: 150,
    },
    {
      key: "analyze.type1",
      name: "Type",
      width: 150,
    },
    {
      key: "analyze.type2",
      name: "Sous-type",
      width: 150,
    },
    {
      key: "analyze.type3",
      name: "Sous-type",
      width: 150,
    },
    {
      key: "isLocatedAt.address.hasAddressCity.label.@fr",
      sortKey: "isLocatedAt.address.hasAddressCity.label.@fr.raw",
      name: "Commune",
      width: 200,
    },
    {
      key: "isLocatedAt.address.postalCode",
      name: "Code postal",
      width: 100,
    },
    {
      key: "isLocatedAt.address.hasAddressCity.insee",
      name: "Code insee",
      width: 100,
    },
    {
      key: "isLocatedAt.address.hasAddressCity.isPartOfDepartment.label.@fr",
      sortKey: "isLocatedAt.address.hasAddressCity.isPartOfDepartment.label.@fr.raw",
      name: "Département",
      width: 200,
    },
    {
      key: "isLocatedAt.address.hasAddressCity.isPartOfDepartment.isPartOfRegion.label.@fr",
      sortKey: "isLocatedAt.address.hasAddressCity.isPartOfDepartment.isPartOfRegion.label.@fr.raw",
      name: "Région",
      width: 200,
    },
    {
      key: "hasContact.homepage",
      name: "Site internet",
      width: 250,
      formatter: ({row, column}: FormatterProps<any>) => {
        const url = extract(row, column.key);
        return (
          <>
            <a href={url} title={url} target="_blank">
              {url}
            </a>
          </>
        );
      },
    },
    {
      key: "hasBeenCreatedBy.legalName",
      name: "Créateur",
      width: 200,
    },
  ];

  const initialColumnsOrder = columns.map(c => c.key);
  const [columnsOrder, setColumnsOrder] = useLocalStorage<string[]>("columnOrder", initialColumnsOrder);

  const HeaderRenderer = (props: HeaderRendererProps<{[p:string]: any}>) => {
    return <DraggableHeaderRenderer {...props} onColumnsReorder={handleColumnsReorder} />;
  }

  const draggableColumns = useMemo(() => {
    const indexOf = (key) => columnsOrder.indexOf(key) >= 0 ? columnsOrder.indexOf(key) : initialColumnsOrder.indexOf(key);
    const reorderedColumns = columns.sort((a, b) => {
      return indexOf(a.key) - indexOf(b.key);
    });
    return reorderedColumns.map(c => {
      if (c.frozen) return c;
      return { ...c, headerRenderer: HeaderRenderer };
    });
  }, [columnsOrder]);


  const handleColumnsReorder = (sourceKey: string, targetKey: string) => {
    const columnsOrder = draggableColumns.map(c => c.key);
    const sourceColumnIndex = columnsOrder.findIndex(key => key === sourceKey);
    const targetColumnIndex = columnsOrder.findIndex(key => key === targetKey);
    const newColumnsOrder = [...columnsOrder];
    newColumnsOrder.splice(targetColumnIndex, 0, newColumnsOrder.splice(sourceColumnIndex, 1)[0]);
    setColumnsOrder(newColumnsOrder);
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <ResultDataGrid
        URLParams={true}
        columns={draggableColumns}
        rowKeyGetter={(row) => row._id}
        pages={10}
        size={100}
        maxResultWindow={10000}
        onRowClick={(row, col) => {
          if (col.key === "label.@fr") {
            onLabelClick(row);
          }
        }}
        {...props}
      />
    </DndProvider>
  );
};

export default CustomResultDataGrid;
