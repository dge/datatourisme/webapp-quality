/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import MultiList, { MultiListProps } from "../../components/MultiList";

type AnomalyMessageMultiListProps = {
  parentValues: string[];
} & Omit<MultiListProps, "dataField">;

/**
 * Specific MultiList to handle specific logic
 */
const AnomalyMessageMultiList = (props: AnomalyMessageMultiListProps) => {
  const { parentValues: values } = props;

  let filter: any = { match_all: {} };
  if (values && values.length) {
    filter = {
      terms: {
        "analyze.anomalies.analyzer": values,
      },
    };
  }

  const defaultQuery = (/*selectedValues: string[], props: any*/) => {
    return {
      aggs: {
        "analyze.anomalies.message": {
          nested: {
            path: "analyze.anomalies",
          },
          aggs: {
            category: {
              filter,
              aggs: {
                messages: {
                  terms: {
                    field: "analyze.anomalies.message",
                    size: 99,
                  },
                  aggs: {
                    parent: {
                      reverse_nested: {},
                    },
                  },
                },
              },
            },
          },
        },
      },
    };
  };

  const customQuery = (selectedValues: string[]) => {
    const must: any[] = [];

    if (values && values.length) {
      must.push({ terms: { "analyze.anomalies.analyzer": values } });
    }

    if (selectedValues.length) {
      must.push({ terms: { "analyze.anomalies.message": selectedValues } });
    }

    return {
      query: {
        nested: {
          path: "analyze.anomalies",
          query: {
            bool: {
              must
            }
          }
        }
      },
    };
  };

  const getOptionsFromState = (state: any, props: {[key: string]: any}) => {
    const { componentId } = props;
    if (state.aggregations[componentId]) {
      return {
        "analyze.anomalies.message": {
          buckets: state.aggregations[componentId]["analyze.anomalies.message"]["category"]["messages"].buckets.map(
            (bucket: any) => {
              return {
                key: bucket.key,
                doc_count: bucket.parent.doc_count,
              };
            }
          ),
        },
      };
    }

    return [];
  }

  return <MultiList
    {...props}
    dataField="analyze.anomalies.message"
    defaultQuery={defaultQuery}
    customQuery={customQuery}
    getOptionsFromState={getOptionsFromState}
  />;
};

export default AnomalyMessageMultiList;
