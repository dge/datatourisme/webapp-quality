/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import ConditionalComponent from "../../components/ConditionalComponent";
import MultiList from "../../components/MultiList";
import RangeMultiList from "../../components/RangeMultiList";
import { analyzersLabel } from "../../variables";
import AnomalyMessageMultiList from "./AnomalyMessageMultiList";

type AggregationsSidebarProps = {} & React.HTMLAttributes<HTMLDivElement>;

const AggregationsSidebar = (props: AggregationsSidebarProps) => {
  return (
    <div {...props}>
      <MultiList
        dataField="analyze.type1"
        title="Types de point d'intérêt"
        componentId="type"
        react={{
          and: [
            "search",
            "anomalyAnalyzer",
            "anomalyMessage",
            "mediaCount",
            "hasBeenCreatedBy",
            "isPartOfRegion",
            "isPartOfDepartment",
          ],
        }}
      />

      <ConditionalComponent deps={["type2", "type"]}>
        {(state: any) => (
          <MultiList
            title="Sous-type niv. 2"
            componentId="type2"
            dataField="analyze.type2"
            react={{
              and: [
                "search",
                "type",
                "anomalyAnalyzer",
                "anomalyMessage",
                "mediaCount",
                "hasBeenCreatedBy",
                "isPartOfRegion",
                "isPartOfDepartment",
              ],
            }}
          />
        )}
      </ConditionalComponent>

      <ConditionalComponent deps={["type3", "type2"]}>
        {(state: any) => (
          <MultiList
            title="Sous-type niv. 3"
            componentId="type3"
            dataField="analyze.type3"
            react={{
              and: [
                "search",
                "type",
                "type2",
                "anomalyAnalyzer",
                "anomalyMessage",
                "mediaCount",
                "hasBeenCreatedBy",
                "isPartOfRegion",
                "isPartOfDepartment",
              ],
            }}
          />
        )}
      </ConditionalComponent>

      <MultiList
        title="Catégorie d'anomalie"
        componentId="anomalyAnalyzer"
        dataField="analyze.anomalies.analyzer"
        showSearch={false}
        size={999}
        labels={analyzersLabel}
        react={{
          and: [
            "search",
            "type",
            "type2",
            "type3",
            "mediaCount",
            "hasBeenCreatedBy",
            "isPartOfRegion",
            "isPartOfDepartment",
          ],
        }}
      />

      <ConditionalComponent deps={["anomalyMessage", "anomalyAnalyzer"]}>
        {(state: any) => (
          <AnomalyMessageMultiList
            title="Anomalie"
            componentId="anomalyMessage"
            parentValues={state.anomalyAnalyzer?.value}
            react={{
              and: [
                "search",
                "type",
                "type2",
                "type3",
                "anomalyAnalyzer",
                "mediaCount",
                "hasBeenCreatedBy",
                "isPartOfRegion",
                "isPartOfDepartment",
              ],
            }}
          />
        )}
      </ConditionalComponent>

      <RangeMultiList
        componentId="mediaCount"
        dataField="analyze.mediaCount"
        title="Nombre de média"
        ranges={{0: { from: 0, to: 1}, 1: {from: 1, to: 11}, 11: {from: 11}}}
        labels={{ 0: "Aucun média", 1: "De 1 à 10", 11: "Plus de 10" }}
        react={{
          and: [
            "search",
            "type",
            "type2",
            "type3",
            "anomalyAnalyzer",
            "anomalyMessage",
            "hasBeenCreatedBy",
            "isPartOfRegion",
            "isPartOfDepartment",
          ],
        }}
      />

      <MultiList
        title="Créateur"
        componentId="hasBeenCreatedBy"
        dataField="hasBeenCreatedBy.legalName"
        showSearch={true}
        size={20}
        react={{
          and: [
            "search",
            "type",
            "type2",
            "type3",
            "anomalyAnalyzer",
            "anomalyMessage",
            "mediaCount",
            "isPartOfRegion",
            "isPartOfDepartment",
          ],
        }}
      />

      <MultiList
        title="Région"
        componentId="isPartOfRegion"
        dataField="isLocatedAt.address.hasAddressCity.isPartOfDepartment.isPartOfRegion.label.@fr.raw"
        showSearch={false}
        size={10}
        react={{
          and: [
            "search",
            "type",
            "type2",
            "type3",
            "anomalyAnalyzer",
            "anomalyMessage",
            "mediaCount",
            "hasBeenCreatedBy"
          ],
        }}
      />

      <ConditionalComponent deps={["isPartOfDepartment", "isPartOfRegion"]}>
        {(state: any) => (
          <MultiList
            title="Département"
            componentId="isPartOfDepartment"
            dataField="isLocatedAt.address.hasAddressCity.isPartOfDepartment.label.@fr.raw"
            showSearch={false}
            size={10}
            react={{
              and: [
                "search",
                "type",
                "type2",
                "type3",
                "anomalyAnalyzer",
                "anomalyMessage",
                "mediaCount",
                "hasBeenCreatedBy",
                "isPartOfRegion",
              ],
            }}
          />
        )}
      </ConditionalComponent>
    </div>
  );
};

export default AggregationsSidebar;
