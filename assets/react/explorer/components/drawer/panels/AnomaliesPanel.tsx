/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { BiError, BiHappy, BiTargetLock } from "react-icons/bi";
import SecureLinkify from "../../../../components/SecureLinkify";
import { analyzers } from "../../../../variables";

const AnomaliesPanel = ({ data }: { data: { [key: string]: any } }) => {
  const { anomalies } = data.analyze;

  const groups: { [key: string]: any } = {};
  anomalies.forEach((a: any) => {
    const key = [a.analyzer, a.message, a.value].join(",");
    if (!groups[key]) {
      groups[key] = {
        key,
        ...a,
        path: [],
      };
    }
    groups[key].path.push(a.path);
  });

  const items = Object.values(groups);

  return (
    <div className="p-3 bg-gray-50 h-full">
      {items.map((item: any) => {
        return (
          <div
            key={item.key}
            className="border border-gray-300 shadow-sm bg-white rounded p-3 flex flex-col justify-between leading-normal mb-3 last:mb-0"
          >
            <div className="mb-3">
              <div className="flex items-center mb-2">
                <BiError className="h-8 w-8 mr-2 text-gray-300" />
                <div className="flex-1">
                  <div className="text-sm text-gray-600 flex items-center leading-none">
                    {analyzers[item.analyzer].label}
                  </div>
                  <div className="text-gray-900 font-bold text-md">{item.message}</div>
                </div>
              </div>
              <div className="">{item.description}</div>
              {item.value && (
                <div className="mt-3 text-gray-700 leading-5 text-sm font-mono">
                  <SecureLinkify>{item.value}</SecureLinkify>
                </div>
              )}
            </div>

            {item.path && (
              <div className="grid grid-cols-2 -mb-1">
                {item.path.map((path: any) => (
                  <div key={path} className="text-gray-600 flex items-center text-sm mb-1">
                    <BiTargetLock className="mr-1" /> {path}
                  </div>
                ))}
              </div>
            )}
          </div>
        );
      })}

      {items.length === 0 && (
        <div className="flex flex-col justify-center items-center h-40 text-gray-400">
          <BiHappy className="h-14 w-14 mb-3" />
          Aucune anomalie n'a été detectée !
        </div>
      )}
    </div>
  );
};

export default AnomaliesPanel;
