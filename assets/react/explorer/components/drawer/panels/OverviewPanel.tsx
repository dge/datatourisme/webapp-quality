/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import React, { HtmlHTMLAttributes, ReactNode, useState } from "react";
import Moment from "react-moment";
import extract, { extractAsArray } from "../../../../utils/extract";
import { DescriptionList, DescriptionListItem } from "../components/DescriptionList";

/**
 * Helper component to handle multiple value in properties
 */
const ItemValue = (props: { separator?: ReactNode } & HtmlHTMLAttributes<HTMLDivElement>) => {
  const { children, separator, ...p } = props;
  if (!React.Children.count(children)) return null;
  if (Array.isArray(children)) {
    return (
      <div {...p}>
        {children.map((c, idx) => (
          <React.Fragment key={idx}>
            {c} {idx < children.length - 1 && (separator ? separator : <br />)}
          </React.Fragment>
        ))}
      </div>
    );
  }

  return <div {...p}>{children}</div>;
};

const DescriptionListIndividual = ({ entities, label, lang }: { entities: any[]; label: string; lang: string }) => {
  if (entities.length === 0) {
    return null;
  }

  return (
    <DescriptionListItem label={label}>
      {entities.map((entity) => (
        <span
          key={entity.uri}
          className="text-sm leading-none inline-block py-1 px-2 rounded text-slate-600 bg-slate-200 last:mr-0 mr-1"
        >
          {entity.label[`@${lang}`]}
        </span>
      ))}
    </DescriptionListItem>
  );
};

/**
 * Overview panel
 */
const OverviewPanel = ({ data }: { data: { [key: string]: any } }) => {
  const [lang, setLang] = useState<string>("fr");

  // label
  const label = extract(data, `label.@${lang}`);

  // label
  const description = extract(data, `hasDescription.description.@${lang}`);

  // types
  const analyze = extract(data, "analyze");
  const types = [...analyze.type1, ...analyze.type2, ...analyze.type3];

  // various data
  const hasTheme: any[] = extractAsArray(data, "hasTheme");
  const hasContact: any[] = extractAsArray(data, "hasContact");
  const isLocatedAtAddress: any[] = extractAsArray(data, "isLocatedAt.address");
  const lastUpdate: string = extract(data, "lastUpdate");
  const lastUpdateDatatourisme: string = extract(data, "lastUpdateDatatourisme");

  // specific data : fma
  const takesPlaceAt: any[] = extractAsArray(data, "takesPlaceAt");

  // specific data : tour
  const hasTourType: any[] = extractAsArray(data, "hasTourType");
  const highDifference: number | null = extract(data, "highDifference");

  // specific data
  const isEquippedWith: any[] = extractAsArray(data, "isEquippedWith");
  const hasReviewValue: any[] = extractAsArray(data, "hasReview.hasReviewValue");

  // offers ?
  // console.log(data);

  // wikidata
  const sameAs = extract(data, "sameAs");

  return (
    <>
      <div className="p-3">
        <div className="text-2xl mb-2">{label}</div>
        {types.map((type) => (
          <span
            key={type}
            className="text-sm leading-none inline-block py-1 px-2 rounded text-slate-600 bg-slate-200 last:mr-0 mr-1"
          >
            {type}
          </span>
        ))}

        {description && <div className="mt-3">{description}</div>}
      </div>
      <DescriptionList>
        {/* Thèmes */}
        <DescriptionListIndividual entities={hasTheme} label="Thèmes" lang={lang} />

        {/* Adresse */}
        {isLocatedAtAddress.length > 0 && (
          <DescriptionListItem label="Adresse">
            {isLocatedAtAddress.map((address) => (
              <div key={address.uri} className="my-3 first:mt-0 last:mb-0">
                <ItemValue children={address.streetAddress} />
                <ItemValue
                  children={
                    <>
                      {address.postalCode} {address.addressLocality}
                    </>
                  }
                />
              </div>
            ))}
          </DescriptionListItem>
        )}

        {/* Equipement */}
        <DescriptionListIndividual entities={isEquippedWith} label="Équipement" lang={lang} />

        {/* Labels */}
        <DescriptionListIndividual entities={hasReviewValue} label="Labels" lang={lang} />

        {/* Dates */}
        {takesPlaceAt.length > 0 && (
          <DescriptionListItem label="Date(s)">
            {takesPlaceAt.map((place) => (
              <div key={place.uri} className="my-3 first:mt-0 last:mb-0">
                {place.startDate && (
                  <ItemValue
                    children={
                      <>
                        <span className="font-semibold">Début</span> :{" "}
                        <Moment format="D MMMM YYYY">{place.startDate}</Moment>
                        {place.startTime ? ` (${place.startTime})` : null}
                      </>
                    }
                  />
                )}
                {place.endDate && (
                  <ItemValue
                    children={
                      <>
                        <span className="font-semibold">Fin</span> :{" "}
                        <Moment format="D MMMM YYYY">{place.endDate}</Moment>
                        {place.endTime ? ` (${place.endTime})` : null}
                      </>
                    }
                  />
                )}
                <ItemValue children={place.email} />
              </div>
            ))}
          </DescriptionListItem>
        )}

        {/* Contacts */}
        {hasContact.length > 0 && (
          <DescriptionListItem label="Contact">
            {hasContact.map((contact) => (
              <div key={contact.uri} className="my-3 first:mt-0 last:mb-0">
                <ItemValue children={contact.legalName} />
                <ItemValue children={contact.telephone} />
                <ItemValue children={contact.email} />
                {contact.homepage && (
                  <a href={contact.homepage} target="_blank" rel="noreferrer">
                    {contact.homepage}
                  </a>
                )}
              </div>
            ))}
          </DescriptionListItem>
        )}

        {/* Type d'itinéraire */}
        <DescriptionListIndividual entities={hasTourType} label="Type d'itinéraire" lang={lang} />

        {/* Dénivellé */}
        {highDifference && <DescriptionListItem label="Dénivellé">{highDifference} m</DescriptionListItem>}

        {/* lastUpdate */}
        {lastUpdate && (
          <DescriptionListItem label="Mise à jour">
            <Moment format="DD/MM/YYYY">{lastUpdate}</Moment>
          </DescriptionListItem>
        )}

        {/* lastUpdateDatatourisme */}
        {lastUpdateDatatourisme && (
          <DescriptionListItem label="Mise à jour DATAtourisme">
            <Moment format="DD/MM/YYYY à HH:mm">{lastUpdateDatatourisme}</Moment>
          </DescriptionListItem>
        )}

        {/* uri */}
        <DescriptionListItem label="URI">
          <a href={data.uri} target="_blank" rel="noreferrer">
            {data.uri}
          </a>
        </DescriptionListItem>

        {/* Wikidata */}
        {sameAs && (
          <DescriptionListItem label="Wikidata">
            <a href={sameAs.uri} target="_blank" rel="noreferrer">
              {sameAs.uri}
            </a>
          </DescriptionListItem>
        )}
      </DescriptionList>
    </>
  );
};

export default OverviewPanel;
