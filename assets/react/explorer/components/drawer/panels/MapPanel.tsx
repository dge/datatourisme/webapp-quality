/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import {useEffect} from "react";

import {Feature} from "ol";
import Map from "ol/Map";
import View from "ol/View";
import {defaults as defaultControls, OverviewMap} from "ol/control";
import {Point} from "ol/geom";
import {fromLonLat} from "ol/proj";
import TileLayer from "ol/layer/Tile";
import VectorLayer from "ol/layer/Vector";
import {OSM, TileWMS, Vector } from "ol/source";
import {Circle, Stroke, Style} from "ol/style";

import {extractAsArray} from "../../../../utils/extract";
import { getDefaultBaseLayer } from "../../../../utils/openlayers";

const MapPanel = ({ data }: { data: { [key: string]: any } }) => {

  const lat: number = extractAsArray(data, "isLocatedAt.geoPoint.lat");
  const lon: number = extractAsArray(data, "isLocatedAt.geoPoint.lon");
  const center = [lon, lat];
  const zoom = 16;

  useEffect(() => {
    // create map

    const overviewMapControl = new OverviewMap({
      layers: [ getDefaultBaseLayer() ],
      collapsed: false
    });

    const map = new Map({
      target: 'map',
      layers: [ getDefaultBaseLayer() ],
      view: new View({
        projection: 'EPSG:3857',
        center: fromLonLat(center),
        zoom
      }),
      controls: defaultControls({
        zoom: true
      }).extend([overviewMapControl]),
    });

    // add marker
    const marker = new VectorLayer({
      source : new Vector({
        features: [
          new Feature({
            geometry: new Point(fromLonLat(center))
          })
        ],
      }),
      style: new Style({
        image: new Circle({
          radius: 8,
          stroke: new Stroke({
            color: [255,0,0], width: 2
          })
        }),
      })
    });
    map.addLayer(marker);
  }, []);

  return (
    <div id="map" className="map-container w-full h-full" />
  );
};

export default MapPanel;
