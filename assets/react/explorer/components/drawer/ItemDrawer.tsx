/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useContext, useEffect, useState } from "react";
import { BiError, BiImage } from "react-icons/bi";

import Drawer, { DrawerProps } from "../../../components/Drawer";
import { Tab, TabList, TabPanel, Tabs } from "../../../components/Tabs";
import extract from "../../../utils/extract";
import { ExplorerContext } from "../../Explorer";
import AnomaliesPanel from "./panels/AnomaliesPanel";
import MapPanel from "./panels/MapPanel";
import OverviewPanel from "./panels/OverviewPanel";
import RawPanel from "./panels/RawPanel";

type ItemDrawerProps = {
  uri: string | undefined;
} & Omit<DrawerProps, "children" | "open">;

type TabValue = "overview" | "map" | "raw" | "anomalies";

const DrawerContent = (props: any) => {
  const { data } = props;
  const [activeTab, setActiveTab] = useState<TabValue>("overview");
  const [imageError, setImageError] = useState<boolean>(false);
  const countAnomaly = data.analyze.anomalies ? data.analyze.anomalies.length : 0;

  // image
  let image = null;
  let images = extract(data, "hasRepresentation");
  if (images) {
    image = Array.isArray(images) ? images[0] : images;
    image = extract(image, "hasRelatedResource.locator");
  }

  return (
    <div className="flex flex-col h-full text-black">
      {image && (
        <img
          className="flex-shrink-0 object-cover h-56 w-full shadow-md"
          src={image}
          alt={image}
          onError={({ target }: any) => {
            setImageError(true);
            target.remove();
          }}
        />
      )}

      {!image && !imageError && (
        <div className="flex flex-col flex-shrink-0 items-center justify-center h-56 w-full bg-gray-200">
          <BiImage className="h-10 w-10 text-gray-400 mb-2" />
          <div className="text-md text-gray-400">aucune image</div>
        </div>
      )}

      {imageError && (
        <div className="flex flex-col flex-shrink-0 items-center justify-center h-56 w-full bg-red-100">
          <BiError className="h-10 w-10 text-red-400 mb-1" />
          <div className="text-md text-red-400">impossible de charger l'image</div>
        </div>
      )}

      <Tabs
        className="flex-1"
        selectedItem={activeTab}
        onChange={(k) => setActiveTab(k)}
      >
        <TabList>
          <Tab className="py-2 px-4" item="overview">Aperçu</Tab>
          <Tab className="py-2 px-4" item="raw">Données brutes</Tab>
          <Tab className="py-2 px-4" item="map">Localisation</Tab>
          <Tab className="py-2 px-4" item="anomalies">Anomalies <span className={`${countAnomaly > 0 ? 'bg-red-600' : 'bg-slate-600'} leading-none text-white py-0 px-2 text-sm rounded-xl`}>{countAnomaly}</span></Tab>
        </TabList>
        <TabPanel item="overview" children={<OverviewPanel data={data} />} />
        <TabPanel item="raw" children={<RawPanel data={data} />} />
        <TabPanel item="map" children={<MapPanel data={data} />} />
        <TabPanel item="anomalies" children={<AnomaliesPanel data={data} />} />
      </Tabs>
    </div>
  );
};



const ItemDrawer = (props: ItemDrawerProps) => {
  const { uri, ...otherProps } = props;
  const { client } = useContext(ExplorerContext);
  const [data, setData] = useState<{ [key: string]: any } | null>(null);

  useEffect(() => {
    if (uri) {
      client.get("datatourisme", { id: uri }).then((data) => {
        setData(data._source);
      });
    } else {
      setData(null);
    }
  }, [uri]);

  return (
    <Drawer open={uri !== undefined} {...otherProps}>
      {!data && <>loading...</>}
      {data && <DrawerContent key={uri} data={data} />}
    </Drawer>
  );


}

export default ItemDrawer
