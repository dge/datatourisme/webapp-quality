/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { MouseEvent, useContext } from "react";
import SearchInput from "../../components/SearchInput";
import { ExplorerContext } from "../Explorer";

type SearchToolbarProps = {
  viewMode: "list" | "map";
  setViewMode: (viewMode: "list" | "map") => void;
} & React.HTMLAttributes<HTMLDivElement>;

const SearchToolbar = (props: SearchToolbarProps) => {
  const { viewMode, setViewMode, className } = props;
  const { currentQuery } = useContext(ExplorerContext);

  // download
  const download = (event: MouseEvent, format: string) => {
    event.preventDefault();
    if (currentQuery) {
      const query = JSON.stringify(currentQuery["query"]);
      const url = `${window.location.origin}/es/datatourisme/export/${query}/${format}`;
      if (url.length > 8177) {
        console.error("Query parameters size is too long");
        return;
      }
      window.location.href = url;
    }
  };

  return (
    <div className={`mr-3 flex ${className}`}>
      <SearchInput componentId="search" textFields={["label.@fr"]} keywordFields={["identifier"]} className="flex-1" />
      <div className="btn-group mx-3">
        <button className={`btn btn-${viewMode == "list" ? "primary" : "white"}`} onClick={() => setViewMode("list")}>
          <span className="fa fa-bars"></span> Liste
        </button>
        <button className={`btn btn-${viewMode == "map" ? "primary" : "white"}`} onClick={() => setViewMode("map")}>
          <span className="fa fa-globe"></span> Carte
        </button>
      </div>
      <div className="btn-group">
        <button type="button" className="btn btn-white dropdown-toggle" data-toggle="dropdown">
          <span className="fa fa-download"></span>
        </button>
        <ul className="dropdown-menu pull-right shadow-lg text-sm">
          <li>
            <a href="#" onClick={(e) => download(e, "csv")}>
              Télécharger en <strong>CSV</strong>
            </a>
          </li>
          <li>
            <a href="#" onClick={(e) => download(e, "xlsx")}>
              Télécharger en <strong>XLSX</strong>
            </a>
          </li>
          <li>
            <a href="#" onClick={(e) => download(e, "ods")}>
              Télécharger en <strong>ODS</strong>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default SearchToolbar;
