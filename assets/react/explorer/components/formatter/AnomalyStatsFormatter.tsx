/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useState } from "react";
import { FormatterProps } from "react-data-grid";
import { DefaultValueFormatter } from "../../../components/ResultDataGrid";
import extract from "../../../utils/extract";
import Popper from "@mui/base/PopperUnstyled";
import { analyzers } from "../../../variables";

const AnomalyStatsFormatter = (props: FormatterProps<any>) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const anomalyStats = extract(props.row, "analyze.anomalyStatistics") || { total: 0};
  const entries = Object.entries(anomalyStats)
    .filter((e) => e[0] != "total")
    .sort((a, b) => {
      return a[1] < b[1] ? 1 : a[1] > b[1] ? -1 : 0;
    });

  const onMouseOver = (e: HTMLElement) => {
    setAnchorEl(e.closest("[role=gridcell]"));
  };

  return (
    <>
      <div
        onMouseOver={(e) => onMouseOver(e.currentTarget)}
        onMouseOut={() => setAnchorEl(null)}
      >
        <DefaultValueFormatter {...props} />
      </div>
      {anomalyStats.total > 0 && (
        <Popper open={open} anchorEl={anchorEl} style={{zIndex: 999}}>
          <table className="table table-condensed w-64 bg-white shadow-lg border border-gray-300">
            <tbody>
              {entries.map((entry) => {
                const [key, value] = entry;
                const { label } = analyzers[key];
                return (
                  <tr key={key}>
                    <td>{label}</td>
                    <td>
                      <strong>{value}</strong>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Popper>
      )}
    </>
  );
};

export default AnomalyStatsFormatter;
