import Linkify, { Props } from "react-linkify";

const SecureLinkify = (props: Props) => {
  return (
    <Linkify
      {...props}
      componentDecorator={(decoratedHref, decoratedText, key) => (
        <a
          target="blank"
          // className="underline text-blue-600 hover:text-blue-800 "
          className="underline"
          rel="noopener noreferrer"
          href={decoratedHref}
          key={key}
        >
          {decoratedText}
        </a>
      )}
    />
  );
};

export default SecureLinkify;
