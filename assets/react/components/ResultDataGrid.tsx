/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useEffect, useState } from "react";
import { ReactiveComponentProps } from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";
import ReactDataGrid, {
  Column,
  DataGridProps as ReactDataGridProps,
  FormatterProps,
  SortColumn,
} from "react-data-grid";
import { ReactiveComponent } from "@appbaseio/reactivesearch";
import Pagination, { PaginationProps } from "./Pagination";
import { ReactiveComponentRenderProps } from "../interfaces";
import extract from "../utils/extract";
import { ExplorerContext } from "../explorer/Explorer";
import Spinner from "./Spinner";

export type ResultDataGridProps = {
  columns: (Column<{ [s: string]: any }, unknown> & { sortKey?: string })[];
  pages?: number;
  maxResultWindow?: number;
  renderPagination?: (props: PaginationProps) => JSX.Element;
} & ReactiveComponentProps &
  Omit<ReactDataGridProps<{ [s: string]: any }>, "rows">;

type ResultDataGridComponentProps = ReactiveComponentRenderProps & ResultDataGridProps;

export const DefaultValueFormatter = (props: FormatterProps<{ [key: string]: any }>) => {
  let value = extract(props.row, props.column.key);
  if (value !== null) {
    value = Array.isArray(value) ? value.join(", ") : value;
    return <span title={value}>{value}</span>;
  }

  return null;
};

/**
 * ResultDataGridComponent
 */
const ResultDataGridComponent = (props: ResultDataGridComponentProps) => {
  const {
    data,
    resultStats,
    loading,
    size,
    columns,
    className,
    defaultColumnOptions,
    setQuery,
    pages = 5,
    value,
    maxResultWindow,
    renderPagination,
    ...gridProps
  } = props;

  // initialize state
  const state: any = value ? JSON.parse(value) : {};
  const [sortColumns, setSortColumns] = useState<readonly SortColumn[]>(state.sort || []);
  const [page, setPage] = useState<number>(state.page || 1);

  // update state when page or sortColumns changed
  useEffect(() => {
    const state: any = {};
    if (page > 1) {
      state.page = page;
    }
    if (sortColumns.length) {
      state.sort = sortColumns;
    }

    if (Object.keys(state).length > 0) {
      setQuery({ value: JSON.stringify(state) });
    } else {
      setQuery({ value: null });
    }
  }, [page, sortColumns]);

  // limit max page according to maxResultWindow parameter
  let maxPage = resultStats.numberOfPages;
  if (maxResultWindow && Math.ceil(maxResultWindow / size!) < maxPage) {
    maxPage = Math.ceil(maxResultWindow / size!);
  }

  // render pagination
  const paginationProps: PaginationProps = {
    currentPage: page,
    maxPage,
    total: resultStats.numberOfResults,
    itemsPerPage: size!,
    pages,
    setPage,
  };

  const _defaultColumnOptions = {
    resizable: true,
    sortable: true,
    formatter: DefaultValueFormatter,
    ...defaultColumnOptions,
  };

  return (
    <div className="h-full flex flex-col">
      <div className="relative flex-1">
        <div className="absolute bottom-0 left-0 right-0 top-0">
          <ReactDataGrid
            rows={data}
            rowHeight={30}
            columns={columns}
            className="h-full"
            sortColumns={sortColumns}
            onSortColumnsChange={setSortColumns}
            defaultColumnOptions={_defaultColumnOptions}
            {...gridProps}
          />
        </div>
        <div
          className={`absolute flex justify-center items-center top-0 left-0 bottom-0 right-0 transition ease-in-out delay-50 bg-white/80 pointer-events-none opacity-${
            loading ? "100" : "0"
          }`}
        >
          <Spinner size={16} className="w-16 h-16"></Spinner>
        </div>
      </div>
      {renderPagination ? renderPagination(paginationProps) : <Pagination {...paginationProps} />}
    </div>
  );
};

/**
 * ResultDataGrid
 */
const ResultDataGrid = (props: ResultDataGridProps) => {
  const { size = 100, columns } = props;

  // build the query, based on state
  const defaultQuery = (value: string) => {
    const state: any = value ? JSON.parse(value) : {};
    const page = state.page || 1;

    const sort = (state.sort || []).map((col: any) => {
      const column = columns.find((column) => column.key === col.columnKey);
      const key = column && column.sortKey ? column.sortKey : col.columnKey;
      return { [key]: { order: col.direction.toLowerCase() } };
    });

    return {
      query: { match_all: {} },
      aggs: {}, // need it to avoid a bug (aggs from other components is injected, even from defaultQuery !)
      size,
      from: (page - 1) * size,
      sort,
      track_total_hits: true,
    };
  };

  return (
    <ReactiveComponent
      defaultQuery={defaultQuery}
      size={size}
      render={(p: ReactiveComponentRenderProps) => <ResultDataGridComponent size={size} {...props} {...p} />}
      {...props}
    />
  );
};

export default ResultDataGrid;
