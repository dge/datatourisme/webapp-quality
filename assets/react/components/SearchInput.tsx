/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { ReactiveComponentProps } from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";
import { ReactiveComponent } from "@appbaseio/reactivesearch";
import { useEffect, useState } from "react";
import useDebounce from "../hooks/useDebounce";
import { ReactiveComponentRenderProps } from "../interfaces";

type SearchInputProps = {
  textFields?: string | string[],
  keywordFields?: string | string[],
  className?: string,
} & ReactiveComponentProps;

const getQuery = (value: string, textField: string | string[] | null, keywordField: string | string[] | null) => {
  const textFields = textField ? (Array.isArray(textField) ? textField : [textField]) : [];
  const keywordFields = keywordField ? (Array.isArray(keywordField) ? keywordField : [keywordField]) : [];
  const should = [];

  if (textFields.length) {
    should.push({
      multi_match: {
        query: value,
        fields: textFields,
        type: "best_fields",
        operator: "or",
        fuzziness: 0,
      },
    });
    should.push({
      multi_match: {
        query: value,
        fields: textFields,
        type: "phrase",
        operator: "or",
      },
    });
    should.push({
      multi_match: {
        query: value,
        fields: textFields,
        type: "phrase_prefix",
        operator: "or",
      },
    });
  }

  keywordFields.forEach(field => {
    should.push({
      terms: {
        [field]: value.split(" ").map(v => v.trim()).filter(value => !!value)
      },
    });
  });

  return { bool: { should } };
}

const SearchInputComponent = (
  props: ReactiveComponentRenderProps & SearchInputProps
) => {
  const { className, textFields, keywordFields, setQuery, value } = props;
  const [inputValue, setInputValue] = useState<string>(value || "");
  const debouncedInputValue: string = useDebounce(inputValue, 500);

  useEffect(() => {
    const query = inputValue ? getQuery(inputValue, textFields, keywordFields) : null;
    setQuery({ value: inputValue, query });
  }, [debouncedInputValue]);

  // handle external update (like SelectedFilters component)
  useEffect(() => {
    setInputValue(value || "");
  }, [value])

  return (
    <div className={`form-group m-0 relative ${className}`}>
      <input
        type="text"
        className="form-control pl-8"
        placeholder="Rechercher..."
        value={inputValue}
        onChange={(e) => setInputValue(e.currentTarget.value)}
      />
      <span className="fa fa-search form-control-feedback text-gray-300 right-auto left-0" />
      { value && <button className="absolute top-0 right-4 leading-[34px]"><span className="fa fa-close" onClick={() => setInputValue("")}/></button> }
    </div>
  );
};

const SearchInput = (props: SearchInputProps) => {
  const { textFields, keywordFields } = props;

  // we need to set the custom query to initialize correctly the component
  // and dependant components
  const customQuery = (value: string) => {
    return value ? {query: getQuery(value, textFields, keywordFields)} : null;
  }

  return (
    <ReactiveComponent
      customQuery={customQuery}
      render={(p: ReactiveComponentRenderProps) => (
        <SearchInputComponent {...props} {...p} />
      )}
      URLParams={true}
      {...props}
    />
  );
};

export default SearchInput;
