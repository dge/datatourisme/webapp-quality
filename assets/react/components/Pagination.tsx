/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

export type PaginationProps = {
  pages: number;
  maxPage: number;
  currentPage: number;
  setPage: (page: number) => void;
  // not used in basic implementation
  total: number;
  itemsPerPage: number;
};

// The main objective is to have this display:
//
// (1) (2) (3) (4) (5) ... (1000)             on page 1
// (1) ... (7) (8) (9) (10) (11) ... (1000)   on page 9
// (1) ... (996) (997) (998) (999) (1000)     on page 1000
//
// X and Y are used to simulate "..." with different keys. Just like my code in 1997.
function buttons(page: number, max: number, window: number) {
  if (page < window || page > max) {
    return [
      ...Array.from(Array(Math.min(max, window)).keys()).map((e) => e + 1),
      ...(max > window + 1 ? ["x", max] : []),
    ];
  } else if (page >= window && page <= max - (window - 1)) {
    return [1, "x", ...Array.from(Array(window).keys()).map((e) => e + 1 + (page - Math.ceil(window / 2))), "y", max];
  } else if (page === window && max === window) {
    return Array.from(Array(window).keys()).map((e) => e + 1);
  }

  return [1, "x", ...Array.from(Array(window).keys()).map((e) => e + 1 + (max - window))];
}

const format = (number: number, options?) => {
  return new Intl.NumberFormat("fr-FT", options).format(number);
};

const Pagination = ({ pages, maxPage, total, itemsPerPage, currentPage, setPage }: PaginationProps) => {
  if (maxPage) {
    const from = itemsPerPage * (currentPage - 1) + 1;
    const to = Math.min(itemsPerPage * currentPage, total);

    return (
      <div className="flex m-3">
        <div className="flex-1">
          Résultats <strong>{format(from)}</strong> à <strong>{format(to)}</strong> sur <strong>{format(total)}</strong>
        </div>
        <nav>
          <ul className="pagination m-0">
            {buttons(currentPage, maxPage, pages)
              .filter((e) => (Number.isInteger(e) ? e <= maxPage : e))
              .map((i) => {
                if (Number.isInteger(i)) {
                  return (
                    <li key={i} className={currentPage === i ? "active" : ""}>
                      <a
                        href="#"
                        onClick={(e) => {
                          setPage(i as number);
                          e.preventDefault();
                        }}
                      >
                        {i}
                      </a>
                    </li>
                  );
                }
                return (
                  <li key={i}>
                    <a>…</a>
                  </li>
                );
              })}
          </ul>
        </nav>
      </div>
    );
  }

  return <></>;
};

export default Pagination;
