/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import ReactDOM from "react-dom";
import Monitor from "./Monitor";
import "./styles.scss";

const element = document.getElementById("react-root");
ReactDOM.render(<Monitor url={element.dataset.url}></Monitor>, element);
