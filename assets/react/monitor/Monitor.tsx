/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import React, { useEffect, useState } from "react";
import { Sparklines, SparklinesLine } from "react-sparklines";
import { FaRegEye } from "react-icons/fa";
import ElasticsearchClient, { SearchTotalHits } from "../utils/es-client";
import { format } from "../utils/number";
import StatsPanel from "./components/StatsPanel";
import EvolutionLabel from "./components/EvolutionLabel";
import { gotoExplorer } from "../utils/goto";
import { analyzers } from "../variables";

export type MonitorContextParams = {
  client: ElasticsearchClient;
};

export type Stats = {
  id: string;
  label: string;
  count: number;
  objectCount: number;
  volume: number;
  stats: Omit<Stats, "stats">[];
};

type HorizonCounts = {
  [key: string]: {
    [key: string]: {
      date: string;
      count: number;
    };
  };
};

export const MonitorContext = React.createContext<MonitorContextParams>(null);

const Monitor = ({ url }: { url: string }) => {
  const client = new ElasticsearchClient(url);

  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<Stats[]>([]);
  const [weeklyEvolution, setWeeklyEvolution] = useState<{ [key: string]: number[] }>({});
  const [horizonCounts, setHorizonCounts] = useState<HorizonCounts>({});

  /**
   * Process raw data
   */
  const processStats = (data: any): Stats[] => {
    const totalHits = (data.hits.total as SearchTotalHits).value;

    return (data.aggregations.anomalies as any).analyzer.buckets.map((b: any) => {
      const { key, doc_count, parent, message } = b;
      const label = analyzers[key].label;
      const objectCount = parent.doc_count;

      const stats = message.buckets.map((b2: any) => {
        const { key, doc_count, parent } = b2;
        const volume = Math.round((parent.doc_count / objectCount) * 10000) / 100;
        return { id: key, label: key, count: doc_count, objectCount: parent.doc_count, volume };
      });

      return {
        id: key,
        label: label || key,
        count: doc_count,
        objectCount,
        volume: Math.round((objectCount / totalHits) * 10000) / 100,
        stats,
      };
    });
  };

  useEffect(() => {
    const promises = [];

    // main
    promises.push(
      client
        .search("datatourisme", {
          size: 0,
          track_total_hits: true,
          body: {
            query: { match_all: {} },
            aggs: {
              anomalies: {
                nested: {
                  path: "analyze.anomalies",
                },
                aggs: {
                  analyzer: {
                    terms: {
                      field: "analyze.anomalies.analyzer",
                    },
                    aggs: {
                      parent: {
                        reverse_nested: {},
                      },
                      // --- message
                      message: {
                        terms: {
                          field: "analyze.anomalies.message",
                        },
                        aggs: {
                          parent: {
                            reverse_nested: {},
                          },
                        },
                      },
                      // ---
                    },
                  },
                },
              },
            },
          },
        })
        .then((data) => {
          setData(processStats(data));
        })
    );

    const range = {
      date: {
        gte: "now-6M",
      },
    };

    const aggs = {
      anomalies: {
        nested: {
          path: "anomalies",
        },
        aggs: {
          analyzer: {
            terms: {
              field: "anomalies.analyzer",
            },
            aggs: {
              count: {
                sum: {
                  field: "anomalies.objectCount",
                },
              },
            },
          },
        },
      },
    };

    // for labels
    promises.push(
      client
        .search("statistics", {
          size: 0,
          body: {
            query: {
              bool: {
                filter: [{ range }],
              },
            },

            aggs: {
              horizon: {
                date_range: {
                  field: "date",
                  format: "yyyy-MM-dd",
                  ranges: [
                    { from: "now-1d/d", to: "now/d", key: "day" },
                    { from: "now-1w/d", to: "now-1w+1d/d", key: "week" },
                    { from: "now-1M/d", to: "now-1M+1d/d", key: "month" },
                    { from: "now-3M/d", to: "now-3M+1d/d", key: "trimester" },
                    { from: "now-6M/d", to: "now-6M+1d/d", key: "semester" },
                  ],
                },
                aggs,
              },
            },
          },
        })
        .then((data) => {
          console.log(data);

          const counts: HorizonCounts = {};
          data.aggregations.horizon.buckets.forEach((b: any) => {
            b.anomalies.analyzer.buckets.forEach(({ key, count }: any) => {
              counts[key] = counts[key] ? counts[key] : {};
              counts[key][b.key] = {
                date: b.from_as_string,
                count: count.value,
              };
            });
          });
          setHorizonCounts(counts);
        })
    );

    // for sparkline
    promises.push(
      client
        .search("statistics", {
          size: 0,
          body: {
            // elasticsearch compatible
            // runtime_mappings: {
            //   day_of_week: {
            //     type: "double",
            //     script: {
            //       source: "emit(doc.date.value.dayOfWeek)"
            //     }
            //   }
            // },
            // query: {
            //   bool: {
            //     filter: {
            //       term: {
            //         day_of_week: 1
            //       }
            //     }
            //   }
            // },

            // opensearch compatible
            query: {
              bool: {
                filter: [
                  { range },
                  {
                    script: {
                      script: {
                        source: "doc.date.value.dayOfWeek == 1", // only monday
                      },
                    },
                  },
                ],
              },
            },

            aggs: {
              date: {
                date_histogram: {
                  field: "date",
                  calendar_interval: "1w",
                  time_zone: "Europe/Paris",
                  min_doc_count: 1,
                },
                aggs,
              },
            },
          },
        })
        .then((data) => {
          const evolution: { [key: string]: number[] } = {};
          data.aggregations.date.buckets
            .flatMap((b: any) => b.anomalies.analyzer.buckets)
            .forEach(({ key, count }: any) => {
              evolution[key] = evolution[key] ? evolution[key] : [];
              evolution[key].push(count.value);
            });
          setWeeklyEvolution(evolution);
        })
    );

    Promise.all(promises).then(() => {
      setLoading(false);
    });
  }, []);

  return (
    <MonitorContext.Provider value={{ client }}>
      <section className="content monitor">
        <div className="flex flex-col">
          <div className="box">
            <div className="box-body p-0">
              <table className="table table-responsive mb-0">
                <thead>
                  <tr>
                    <th>Catégorie</th>
                    <th>Volume de POI</th>
                    <th></th>
                    <th>Jour</th>
                    <th>Semaine</th>
                    <th>Mois</th>
                    <th>Trimestre</th>
                    <th>Semestre</th>
                    <th>Historique</th>
                  </tr>
                </thead>
                <tbody>
                  {data.map((stats) => {
                    const Icon = analyzers[stats.id].icon;
                    return (
                      <tr key={stats.id}>
                        <td className="p-3 whitespace-nowrap align-middle">
                          <div className="flex items-start">
                            <Icon className="flex-shrink-0 h-8 w-8 text-gray-500" />
                            <div className="ml-3">
                              <div>{stats.label}</div>
                              <div className="text-gray-500">
                                <strong>{format(stats.count)}</strong> anomalies détectés
                              </div>
                            </div>
                          </div>
                        </td>
                        <td className="p-3 whitespace-nowrap align-middle">
                          <StatsPanel stats={stats} />
                        </td>
                        <td className="p-3 whitespace-nowrap align-middle text-sm">
                          <button onClick={() => gotoExplorer({ anomalyAnalyzer: [stats.id] })} className="px-2 py-1 inline-flex items-center rounded-sm bg-gray-100 hover:bg-gray-200 text-gray-800">
                            <FaRegEye className="mr-1" /> <div>Voir la liste</div>
                          </button>
                        </td>
                        <td className="p-3 whitespace-nowrap align-middle text-sm">
                          <EvolutionLabel
                            origin={horizonCounts?.[stats.id]?.day?.count}
                            target={stats.objectCount}
                            date={horizonCounts?.[stats.id]?.day?.date}
                          />
                        </td>
                        <td className="p-3 whitespace-nowrap align-middle text-sm">
                          <EvolutionLabel
                            origin={horizonCounts?.[stats.id]?.week?.count}
                            target={stats.objectCount}
                            date={horizonCounts?.[stats.id]?.week?.date}
                          />
                        </td>
                        <td className="p-3 whitespace-nowrap align-middle text-sm">
                          <EvolutionLabel
                            origin={horizonCounts?.[stats.id]?.month?.count}
                            target={stats.objectCount}
                            date={horizonCounts?.[stats.id]?.month?.date}
                          />
                        </td>
                        <td className="p-3 whitespace-nowrap align-middle text-sm">
                          <EvolutionLabel
                            origin={horizonCounts?.[stats.id]?.trimester?.count}
                            target={stats.objectCount}
                            date={horizonCounts?.[stats.id]?.trimester?.date}
                          />
                        </td>
                        <td className="p-3 whitespace-nowrap align-middle text-sm">
                          <EvolutionLabel
                            origin={horizonCounts?.[stats.id]?.semester?.count}
                            target={stats.objectCount}
                            date={horizonCounts?.[stats.id]?.semester?.date}
                          />
                        </td>
                        <td className="px-4 py-0 whitespace-nowrap align-bottom">
                          <div style={{ marginBottom: "-2px" }}>
                            <Sparklines data={weeklyEvolution[stats.id]} svgHeight={45} limit={24}>
                              <SparklinesLine color="#2563eb" style={{ strokeWidth: 1 }} />
                            </Sparklines>
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
    </MonitorContext.Provider>
  );
};

export default Monitor;
