

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import moment from "moment";
import { format } from "../../utils/number";

type EvolutionLabelProps = {
  origin: number;
  target: number;
  date: string;
} & React.HTMLAttributes<HTMLDivElement>;

const EvolutionLabel = ({ origin, target, date }: EvolutionLabelProps) => {
  // const to = data.filter((d) => d.id === analyzer)[0]?.objectCount;
    if (origin && target) {
      const evolution = Math.round(((target - origin) / origin) * 100);
      const color = evolution < 0 ? "green" : evolution > 0 ? "red" : "gray";
      // className='text-red-800 text-green-800 text-gray-800'
      // className='bg-red-100 bg-green-100 bg-gray-100'
      // className='border-red-800 border-green-800 border-gray-800'
      return (
        <span
          className={`px-2 inline-flex items-center leading-5 font-semibold rounded-full bg-${color}-100 text-${color}-800`}
          title={`${moment(date).format("DD/MM/YYYY")} : ${format(origin)}`}
        >
          {evolution < 0 && (
            <div className={`border-x-4 border-x-transparent border-t-4 mr-1 border-dashed border-${color}-800`}></div>
          )}
          {evolution > 0 && (
            <div className={`border-x-4 border-x-transparent border-b-4 mr-1 border-dashed border-${color}-800`}></div>
          )}
          {evolution === 0 && (
            <div className={`border-y-4 border-y-transparent border-l-4 mr-1 border-dashed border-gray-400`}></div>
          )}
          {Math.abs(evolution)}%
        </span>
      );
    }

    return (
      <span className="px-2 inline-flex items-center leading-5 font-semibold rounded-full bg-gray-50 text-gray-300">
        <span className="opacity-0">+ 99%</span>
      </span>
    );
};

export default EvolutionLabel
