

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

type ProgressBarProps = {
  value: number
} & React.HTMLAttributes<HTMLDivElement>;

const ProgressBar = ({ value, className, color }: ProgressBarProps) => {
  let c = "bg-sky-600";
  if (value > 30) {
    c = "bg-yellow-600";
  }
  if (value > 50) {
    c = "bg-orange-600";
  }
  if (value > 75) {
    c = "bg-red-600";
  }
  return (
    <div className={`w-full bg-gray-200 rounded-lg h-2.5 ${className ? className : ''}`}>
      <div className={`${color ? color : c} h-full rounded-lg`} style={{ width: `${value}%` }}></div>
    </div>
  );
};

export default ProgressBar
