/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useState } from "react";
import { format } from "../../utils/number";
import { Stats } from "../Monitor";
import ProgressBar from "./ProgressBar";
import Popper from "@mui/base/PopperUnstyled";

type StatsPanelProps = {
  stats: Stats
} & React.HTMLAttributes<HTMLDivElement>;

const StatsPanel = ({ stats }: StatsPanelProps) => {
  const [anchorEl, setAnchorEl] = useState<Element | null>(null);
  const open = Boolean(anchorEl);

  return (
    <div className="flex flex-col w-48" onMouseOver={(e) => setAnchorEl(e.currentTarget.closest("td"))} onMouseOut={() => setAnchorEl(null)}>
      <div className="flex items-center">
        <div className="flex-1">{format(stats.objectCount)}</div>
        <div className="ml-1 text-xs text-gray-500">{format(stats.volume)}%</div>
      </div>
      <ProgressBar value={stats.volume} />

      <Popper open={open} anchorEl={anchorEl} placement="right-start" style={{ zIndex: 999 }}>
        <div className="w-64 p-3 bg-white shadow-lg border border-gray-300">
          {stats.stats.map((stats) => (
            <div key={stats.id} className="mb-3 last:mb-0">
              <div className="flex mb-1 text-sm">
                <div className="flex-1">{stats.label}</div>
                <div className="text-gray-500">{format(stats.volume)}%</div>
              </div>
              <ProgressBar value={stats.volume} className="h-2" color="bg-sky-600" />
            </div>
          ))}
        </div>
      </Popper>
    </div>
  );
};

export default StatsPanel
