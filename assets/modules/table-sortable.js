/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */
'use strict';

import $ from "jquery";

$(function () {
    // on submit : add sort & direction to filters
    $(document).on('click', 'th.sortable', function (e) {
        $("a:first", this)[0].click();
    });
    $(document).on('click', 'th.sortable a', function (e) {
        e.stopImmediatePropagation();
    });

    $.fn.initComponents.add((dom) => {
        // add sorted class to column
        $('th.sortable[class~="desc"], th.sortable[class~="asc"]', dom).each(function () {
            $('table tbody td:nth-child(' + ($(this).index() + 1) + ')').addClass("sorted")
        })
    });
});
