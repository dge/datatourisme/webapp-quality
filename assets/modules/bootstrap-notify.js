/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */
'use strict';

import $ from "jquery";
import "bootstrap-notify";

const originalNotify = $.notify;

$.notify = (options, settings) => {
    settings = $.extend({
        position: 'absolute',
        element: '.notify-container',
        offset: 20,
        z_index: 1029,
        placement: {
            from: 'top',
            align: 'right'
        },
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        //template: '<div data-notify="container" class="col-xs-11 col-sm-4 col-lg-3 alert alert-{0}" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button><span data-notify="icon"></span> <span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>'
    }, settings);

    /*if($(".main-header").length > 0) {
        settings = $.extend({
            element: '.main-header',
            offset: {y: 70, x: 20}
        }, settings);
    } else {
        settings = $.extend({
            element: '.content-wrapper',
            offset: 20
        }, settings);
    }
    console.log(settings);*/

    originalNotify(options, settings);
};

/**
 * handle .bootstrap-notify-alert
 */
$.fn.initComponents.add((dom) => {
    $('.bootstrap-notify-alert', dom).each(function() {
        var settings = $(this).data();
        settings.message = $(this).html();
        $.notify(settings, settings);
    });
});
