
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

'use strict';

import $ from "jquery";

/**
 * $.fn.initComponents
 *
 * Use to initialize jQuery components
 */
$.fn.initComponents = function() {
    return this.each(function() {
        // Do something to each element here.
        for(var i=0; i<$.fn.initComponents.callbacks.length; i++) {
            $.fn.initComponents.callbacks[i](this);
        }
    });
};
$.fn.initComponents.callbacks = [];
$.fn.initComponents.add = (callback) => {
    $.fn.initComponents.callbacks.push(callback);
};

// init app
$(($) => {
  $("body").initComponents();
});

