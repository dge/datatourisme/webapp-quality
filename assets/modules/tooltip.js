/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */
'use strict';

import $ from "jquery";
import "tooltipster";

$.fn.initComponents.add((dom) => {
    $('[data-tooltip]', dom).each(function () {
        $(this).tooltipster({
            theme: 'tooltipster-borderless',
            maxWidth: 300,
            delay: 100,
            distance: 5,
            debug: false,
            side: 'bottom',
            functionInit: (instance, helper) => {
                var $origin = $(helper.origin),
                    dataOptions = $origin.attr('data-tooltip');

                if(dataOptions) {
                    dataOptions = JSON.parse(dataOptions);
                } else {
                    dataOptions = {};
                }

                var data = $(helper.origin).data();
                for(var key in data) {
                    if(key !== "tooltip" && key.indexOf("tooltip") == 0 && key.indexOf("tooltipster") == -1) {
                        var option = key.substr(7,1).toLowerCase() + key.substr(8);
                        dataOptions[option] = data[key];
                    }
                }

                $.each(dataOptions, (name, option) => {
                    instance.option(name, option);
                });
            }
        });
    });
});
