/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */
'use strict';

import $ from "jquery";

$(function () {
    // on submit : add sort & direction to filters
    $(document).on('submit', 'form:has(.table-filters)', function (e) {
        var keys = ['sort', 'direction'];
        for(var i=0; i<keys.length; i++) {
            var value = $.urlParam(keys[i]);
            if(value) {
                $('<input type="hidden">').attr('name', keys[i]).val(value).appendTo(this);
            }
        }
    });

    // on enter : submit form
    $(document).on('keypress', '.table-filters input', function (e) {
        if (e.which == 13) {
            $(this).parents('form:first').trigger('submit');
            return false;
        }
    });

    // on select change : submit form
    $(document).on('change', '.table-filters select', function (e) {
        $(this).parents('form:first').trigger('submit');
        return false;
    });

});

/**
 * Return param from URL
 *
 * @param name
 * @returns {*}
 */
$.urlParam = (name) => {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}
