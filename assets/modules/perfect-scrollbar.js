/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */
'use strict';

import PerfectScrollbar from 'perfect-scrollbar';

/**
 * handle perfect-scrollbar
 */
$.fn.initComponents.add((dom) => {
    $('[data-scrollbar]', dom).each(function() {
      new PerfectScrollbar($(this).get(0));
    });
});
