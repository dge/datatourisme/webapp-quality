/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import "./styles/app.scss";

/** vendors **/
import "bootstrap";

/** modules **/
import "./modules";
import "./modules/modal";
import "./modules/bootstrap-notify";
import "./modules/table-filters";
import "./modules/table-sortable";
import "./modules/conditional-visibility";
import "./modules/tooltip";
import "./modules/selectize";
import "./modules/perfect-scrollbar";
