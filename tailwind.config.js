module.exports = {
  content: [
    "./templates/**/*.twig",
    "./assets/react/**/*.{ts,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        'dge-primary': '#c02846',
        'dge-gray': '#5a5a5a',
        'dge-gray-light': '#878787',
        'dge-blue': '#116390',
        'dge-aqua': '#00a6bd',
        'dge-green': '#82c75d',
        'dge-orange': '#efa500',
        'dge-pink': '#ee7d6c',
        'dge-red': '#e3343e',
        'dge-conjoncture-1': '#a19288',
        'dge-conjoncture-2': '#bfa59a',
        'dge-tourisme': '#8db6b4',
        'dge-prospective': '#86a5b9',
        'dge-evaluation': '#abc3a9'
      }
    },
  },
  plugins: [],
  important: true,
}
