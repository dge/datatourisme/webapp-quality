<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Validator\Constraints;

use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;
use Symfony\Component\Validator\Constraint;

class StrongPassword extends Constraint
{
    const TOO_SHORT = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce1999';
    const TOO_WEAK = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce1998';
    const CONTAINS_FORBIDDEN_PROPERTIES = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce1997';
    const CONTAINS_OLD_PASSWORD = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce1996';
    const TOO_LONG = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce1995';

    protected static $errorNames = [
        self::TOO_SHORT => 'TOO_SHORT_ERROR',
        self::TOO_WEAK => 'TOO_WEAK_ERROR',
        self::CONTAINS_FORBIDDEN_PROPERTIES => 'CONTAINS_FORBIDDEN_PROPERTIES_ERROR',
        self::CONTAINS_OLD_PASSWORD => 'CONTAINS_OLD_PASSWORD_ERROR',
        self::TOO_LONG => 'TOO_LONG_ERROR',
    ];

    public $user = null;

    public $minLength = 14;
    public $shortMessage = 'The password must contains at least %minLength% characters';

    public $maxLength =  PasswordHasherInterface::MAX_PASSWORD_LENGTH;
    public $longMessage = 'The password must contains at maximum %maxLength% characters';

    public $minRequirementsCount = 3;
    public $weakMessage = 'The password is not strong enough';

    public $forbiddenProperties = ['firstName', 'lastName'];
    public $forbiddenMessage = 'The password should not contain the following words : %words%';

    public $oldPasswordMessage = 'The password has to be different of previous passwords';

    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}
