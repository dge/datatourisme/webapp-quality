<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Validator\Constraints;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class StrongPasswordValidator extends ConstraintValidator
{
    private $security;
    private $passwordHasherFactory;

    public function __construct(Security $security, PasswordHasherFactoryInterface $passwordHasherFactory)
    {
        $this->security = $security;
        $this->passwordHasherFactory = $passwordHasherFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($password, Constraint $constraint)
    {
        if (!$constraint instanceof StrongPassword) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\StrongPassword');
        }

        $user = $constraint->user ? $constraint->user : $this->security->getUser();

        if (!$user instanceof UserInterface) {
            throw new ConstraintDefinitionException('The User object must implement the UserInterface interface.');
        }

        // La longueur minimale d'un mot de passe est de 14 caractères
        if (strlen($password) < $constraint->minLength) {
            $this->context->buildViolation($constraint->shortMessage)
                ->setParameter('%minLength%', $constraint->minLength)
                ->setCode(StrongPassword::TOO_SHORT)
                ->addViolation();

            return;
        }

        $passwordHasher = $this->passwordHasherFactory->getPasswordHasher($user);
        $maxPasswordLength = $passwordHasher ? $passwordHasher::MAX_PASSWORD_LENGTH : $constraint->maxLength;
        if (strlen($password) > $maxPasswordLength) {
            $this->context->buildViolation($constraint->longMessage)
                ->setParameter('%maxLength%', $maxPasswordLength)
                ->setCode(StrongPassword::TOO_LONG)
                ->addViolation();

            return;
        }

        $requirementsCount = 0;

        // contenir une lettre minuscule
        if (preg_match('/[a-z]/', $password)) {
            ++$requirementsCount;
        }

        // contenir une lettre majuscule
        if (preg_match('/[A-Z]/', $password)) {
            ++$requirementsCount;
        }

        // contenir un nombre
        if (preg_match('/[0-9]/', $password)) {
            ++$requirementsCount;
        }

        // contenir un des caractères suivants : + - * / , ; : ? . ! = % $ & " ' ( _ ) @ # { } | \ [ ] ;
        if (false !== strpbrk($password, '+-*/,;:?.!=%$&"\\\'(_)@#{}|\[]')) {
            ++$requirementsCount;
        }

        if ($requirementsCount < $constraint->minRequirementsCount) {
            $this->context->buildViolation($constraint->weakMessage)
                ->setCode(StrongPassword::TOO_WEAK)
                ->addViolation();

            return;
        }

        // ne pas inclure le prénom et/ou le nom de l’utilisateur
        $accessor = PropertyAccess::createPropertyAccessor();
        $translitPassword = $this->transliterate($password);

        $values = [];
        foreach ($constraint->forbiddenProperties as $name) {
            $values[] = $accessor->getValue($user, $name);
        }
        $values = array_filter($values);

        foreach ($values as $value) {
            $regex = preg_quote(strtolower($this->transliterate($value)));
            if ($value && preg_match('/' . $regex . '/ui', $translitPassword)) {
                $this->context->buildViolation($constraint->forbiddenMessage)
                    ->setParameter('%words%', join(', ', $values))
                    ->setCode(StrongPassword::CONTAINS_FORBIDDEN_PROPERTIES)
                    ->addViolation();

                return;
            }
        }

        // être différent des mots de passe précédemment utilisés
        // foreach ($user->getPreviousPasswords() as $previousPassword) {
        //     if ($encoder->isPasswordValid($previousPassword, $password, $user->getSalt())) {
        //         $this->context->buildViolation($constraint->oldPasswordMessage)
        //             ->setCode(StrongPassword::CONTAINS_OLD_PASSWORD)
        //             ->addViolation();

        //         return;
        //     }
        // }
    }

    /**
     * @param $str
     *
     * @return string
     */
    private function transliterate($str)
    {
        $transliterator = \Transliterator::create(
            'NFD; [:Nonspacing Mark:] Remove; NFC;'
        );

        return $transliterator->transliterate($str);
    }
}
