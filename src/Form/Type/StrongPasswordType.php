<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Type;

use App\Validator\Constraints\StrongPassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class StrongPasswordType extends AbstractType
{
    public function getParent()
    {
        return RepeatedType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'translation_domain' => 'forms',
                'user' => null,
                'min_length' => 14,
                'type' => PasswordType::class,
                'label' => 'strong_password.label',
                'first_options' => [
                    'label' => 'strong_password.new_password',
                ],
                'second_options' => [
                    'label' => 'strong_password.repeat_new_password',
                ],
                'invalid_message' => 'non_matching_passwords',
                'constraints' => function (Options $options) {
                    return [
                        new NotBlank(['message' => 'empty_password']),
                        new StrongPassword([
                            'user' => $options['user'],
                            'minLength' => $options ['min_length'], //temp
                            'shortMessage' => 'password_too_short',
                            'longMessage' => 'password_too_long',
                            'weakMessage' => 'password_too_weak',
                            'forbiddenMessage' => 'password_forbidden',
                            'oldPasswordMessage' => 'password_not_same_as_old',
                        ]),
                    ];
                },
            ])
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars = array_merge($view->vars, [
            'min_length' => $options['min_length']
        ]);
    }
}
