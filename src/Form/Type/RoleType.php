<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class RoleType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var RoleHierarchyInterface
     */
    private $roleHierarchy;

    /**
     * UserVoter constructor.
     *
     * @param $roleHierarchy
     */
    public function __construct(Security $security, RoleHierarchyInterface $roleHierarchy)
    {
        $this->security = $security;
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $roles = [];

        // get user reachable roles
        $user = $this->security->getUser();
        if ($user instanceof UserInterface) {
            $roles = $this->roleHierarchy->getReachableRoleNames($user->getRoles());
            $roles = array_combine($roles, $roles);
            $roles = array_reverse($roles);
        }

        $resolver
            ->setDefaults([
                'label' => 'Rôle',
                'choices' => $roles,
            ]);

        $resolver->addAllowedTypes('choice_label', 'array');
        $resolver->setNormalizer('choice_label', function (Options $options, $labels) {
            if (is_array($labels)) {
                return function ($value, $key, $index) use ($labels) {
                    return isset($labels[$value]) ? $labels[$value] : $value;
                };
            }

            return $labels;
        });
    }
}
