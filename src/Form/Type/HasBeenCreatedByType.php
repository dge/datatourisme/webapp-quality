<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Type;

use App\Entity\Creator;
use App\Entity\Producer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HasBeenCreatedByType extends AbstractType
{
    protected EntityManagerInterface $em;

    /**
     * __construct
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getParent()
    {
        return ChoiceType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'producer' => null,
                'multiple' => true,
                'expanded' => false,
                'attr' => ['data-selectize' => ''],
                'choice_loader' => function (Options $options) {
                    return ChoiceList::lazy($this, function () use ($options) {
                        return $this->choiceLoader($options['producer']);
                    }, $options['producer']);
                }
            ])
        ;
    }

    /**
     * Load choices based on a producer
     */
    private function choiceLoader(?Producer $producer): array
    {
        $qb = $this->em->createQueryBuilder()
            ->select('c')
            ->from(Creator::class, 'c')
            ->orderBy('c.legalName', 'ASC');

        if ($producer) {
            $qb->where("c.producer = :producer")
                ->setParameter('producer', $producer);
        }

        $options = [];
        $creators = $qb->getQuery()->execute();
        foreach ($creators as $creator) {
            $options[$creator->getLegalName()] = $creator->getUri();
        }

        return $options;
    }
}
