<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Elasticsearch;

use App\Entity\User;
use Exception;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
* AbstractIndex index
*/
abstract class AbstractIndex
{
    private HttpClientInterface $client;

    private Security $security;

    /**
    * Constructor
    */
    public function __construct(
        string $url,
        string $username,
        string $password,
        Security $security
    ) {
        // initialize client
        $this->client = HttpClient::createForBaseUri($url, [
            'auth_basic' => [$username, $password]
        ]);
        $this->security = $security;
    }

    /**
     * Get the client
     */
    public function getClient(): HttpClientInterface
    {
        return $this->client;
    }

    /*
    * Indice name
    */
    abstract public function getIndice(): string;

    /**
     * Alter a query with custom conditions
     */
    public function alterQuery(object &$query, User $user): void
    {
    }

    /**
     * Inject conditions from user data into a query string
     */
    public function alterQueryString(string $queryString): string
    {
        $obj = json_decode($queryString);
        if (!$obj) {
            throw new Exception("Unable to parse the query string : $queryString");
        }
        $this->alterQuery($obj, $this->getUser());
        return json_encode($obj);
    }

    /**
     * Proxy a Request to the ES index
     */
    public function proxy(Request $request, string $path = '_search', $alter = true): Response
    {
        $op = explode("/", $path)[0];
        if (!in_array($op, ['_msearch', '_search', '_count', '_doc'])) {
            throw new \InvalidArgumentException("Invalid operation : $op");
        }

        $path = $this->getIndice() . '/' . $path;
        $response = new StreamedResponse();
        $content = $request->getContent();

        // alter query from user data
        if ($content && $alter) {
            $content = $this->alterQueryContent($content);
        }

        // headers
        $headers = ['content-length' => [strlen($content)]] + $request->headers->all();

        // proxy request
        $proxiedResponse = $this->client->request($request->getMethod(), $path, [
            'headers' => $headers,
            'body' => $content
        ]);

        // Responses are lazy: this code is executed as soon as headers are received
        if (200 !== $proxiedResponse->getStatusCode()) {
            throw new HttpException($proxiedResponse->getStatusCode(), $proxiedResponse->getContent(false));
        }

        // set headers
        $headers = $proxiedResponse->getHeaders();
        $response->headers->set('content-type', $headers['content-type']);
        $response->headers->set('content-encoding', $headers['content-encoding']);

        $response->setCallback(function () use ($response, $proxiedResponse) {
            foreach ($this->client->stream($proxiedResponse) as $chunk) {
                print $chunk->getContent();
            }
        });

        $response->headers->set('Content-Encoding', 'gzip');

        return $response;
    }

    /**
     * Alter one or multiple query strings from request content
     */
    protected function alterQueryContent(string $content): string
    {
        $lines = [];
        foreach (preg_split("/((\r?\n)|(\r\n?))/", $content) as $line) {
            if (trim($line)) {
                $lines[] = $this->alterQueryString(trim($line));
            }
        }
        return join("\n", $lines) . "\n";
    }

    /**
     * Get the current user
     */
    protected function getUser(): User
    {
        return $this->security->getUser();
    }
}
