<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Elasticsearch;

use App\Entity\User;

/**
* Datatourisme index
*/
class DatatourismeIndex extends AbstractIndex
{
    public function getIndice(): string
    {
        return "datatourisme";
    }

    /**
     * Specific conditions
     *
     * @return array|null
     */
    public function alterQuery(object &$query, User $user): void
    {
        if (isset($query->query) && $user->getHasOrganizationIdentifier()) {
            $must = [
                $query->query,
                ["term" => ["hasOrganizationIdentifier" => ["value" => $user->getHasOrganizationIdentifier()]]]
            ];

            if ($user->getHasBeenCreatedBy()) {
                $must[] = ["terms" => ["hasBeenCreatedBy.uri" => $user->getHasBeenCreatedBy()]];
            }

            $query->query = ["bool" => ["must" => $must]];
        }
    }
}
