<?php

/**
 * This file is part of the DATAtourisme project.
 *
 * @author Conjecto <contact@conjecto.com>
 *
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

/**
 * Class NewsRepository.
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, News::class);
    }

    public function findByYearMonth($year, $month)
    {
        $sql = "
            SELECT news.*
            FROM news
            WHERE EXTRACT(YEAR FROM date) = ? AND EXTRACT(MONTH FROM date) = ?
            ORDER BY date DESC, id DESC
        ";
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata(News::class, 'f');
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $query->setParameters([$year, $month]);
        return $query->getResult();
    }

    /**
     * get month/year distribution
     *
     * @return mixed
     */
    public function getMonthYearDistribution()
    {
        $em = $this->getEntityManager();
        $sql = "
            SELECT to_char( date, 'YYYY-MM'), count(id)
            FROM news
            GROUP BY to_char( date, 'YYYY-MM')
            ORDER BY to_char( date, 'YYYY-MM') DESC
        ";
        try {
            $stmt = $em->getConnection()->prepare($sql);
            $result = $stmt->executeQuery();
            return $result->fetchAllKeyValue();
        } catch (Exception $e) {
            return null;
        }
    }
}
