<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Command;

use Elastica\Client;
use Elastica\Index;
use Elastica\Search;
use Elastica\Query;
use Elastica\Query\Exists;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class MapCommand extends Command
{
    private Client $client;

    private Filesystem $filesystem;

    private const PATH = 'var/files/map';

    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
        $this->filesystem = new Filesystem();
    }

    protected function configure(): void
    {
        parent::configure();
        $this
            ->setName('app:cron:map')
            ->setDescription('Préparation des données de la carte de chaleur du tableau de bord');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->filesystem->remove(self::PATH);
        $this->filesystem->mkdir(self::PATH);
        $this->filesystem->mkdir(self::PATH . "/producers");
        $this->filesystem->mkdir(self::PATH . "/creators");

        // $index = new Index($this->client, "datatourisme");
        $search = new Search($this->client);
        $search->addIndex('datatourisme');

        $exists = new Exists("isLocatedAt.geoPoint");
        $query = new Query($exists);
        $query->setSource(['label.@fr', 'isLocatedAt.geoPoint', 'hasOrganizationIdentifier', 'hasBeenCreatedBy.uri']);
        $query->setSort(['_doc']);
        $query->setSize(500);

        $search->setQuery($query);
        $progressBar = new ProgressBar($output);
        $progressBar->start();

        /** @var ResultSet $resultSet */
        $scroll = $search->scroll();
        foreach ($scroll as $scrollId => $resultSet) {
            $progressBar->setMaxSteps($scroll->current()->getTotalHits());
            foreach ($resultSet as $hit) {
                $source = $hit->getSource();
                $data = [
                    'label' => $source['label']['@fr'],
                    'lon' => $source['isLocatedAt']['geoPoint']['lon'],
                    'lat' => $source['isLocatedAt']['geoPoint']['lat'],
                ];
                $this->appendToGlobalFile($data);
                $this->appendToProducerFile($data, $source['hasOrganizationIdentifier']);
                $this->appendToCreatorFile($data, $source['hasBeenCreatedBy']['uri']);
            }
            $progressBar->advance(count($resultSet));
        }

        $this->finalizeFiles();

        // ensures that the progress bar is at 100%
        $progressBar->finish();

        return Command::SUCCESS;
    }

    private function appendToGlobalFile($data)
    {
        $file = self::PATH . '/data.json';
        $this->appendToFile($file, $data);
    }

    private function appendToProducerFile($data, $hasOrganizationIdentifier)
    {
        $file = self::PATH . '/producers/' . $hasOrganizationIdentifier . '.json';
        $this->appendToFile($file, $data);
    }

    private function appendToCreatorFile($data, $uri)
    {
        $file = self::PATH . '/creators/' . md5($uri) . '.json';
        $this->appendToFile($file, $data);
    }

    private function appendToFile($file, $data)
    {
        $first = !$this->filesystem->exists($file);
        $txt = $first ? '[' : ',';
        $txt = $txt . json_encode($data);
        $this->filesystem->appendToFile($file, $txt);
    }

    private function finalizeFiles()
    {
        $finder = new Finder();
        $finder->name('*.json');
        foreach ($finder->in(self::PATH) as $file) {
            $this->filesystem->appendToFile($file, ']');
        }
    }
}
