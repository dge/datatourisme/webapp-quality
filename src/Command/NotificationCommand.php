<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Command;

use App\Notification\AnomalyReportNotification;
use App\Repository\UserRepository;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Elastica\Client;
use Elastica\Search;
use Elastica\Query;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Notifier\NotifierInterface;

class NotificationCommand extends Command
{
    private EntityManagerInterface $em;
    private Client $client;
    private NotifierInterface $notifier;
    private UserRepository $userRepository;

    public function __construct(EntityManagerInterface $em, Client $client, NotifierInterface $notifier, UserRepository $userRepository)
    {
        parent::__construct();
        $this->client = $client;
        $this->em = $em;
        $this->notifier = $notifier;
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        parent::configure();
        $this
            ->setName('app:cron:notification')
            ->setDescription('Envoi des notification aux utilisateurs.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $date = new DateTime();

        // daily notifications (every day)
        $output->writeln('Traitement des notifications quotidiennes');
        $this->sendPeriodicNotifications('daily');

        // weekly notifications (on mondays)
        if ($date->format('w') == 1) {
            $output->writeln('Traitement des notifications hebdomadaires');
            $this->sendPeriodicNotifications('weekly');
        }

        // monthly notifications (on the first day of the month)
        if ($date->format('j') == 1) {
            $output->writeln('Traitement des notifications mensuelles');
            $this->sendPeriodicNotifications('monthly');
        }

        // threshold alerts notifications
        $output->writeln('Traitement des notifications sur seuil');
        $this->sendThresholdNotifications();

        return Command::SUCCESS;
    }

    private function sendPeriodicNotifications(string $frequency)
    {
        $trendQuery = [
            'size' => 0,
            'query' => [
                'bool' => [
                    'filter' => [
                        ['range' => [ 'date' => [ 'gte' => 'now-6M' ] ]]
                    ],
                ],
            ],
            'aggs' => [
                'horizon' => [
                    'date_range' => [
                        'field' => "date",
                        'format' => "yyyy-MM-dd",
                        'ranges' => [
                            [ 'from' => "now/d", 'to' => "now+1d/d", 'key' => "today" ],
                            [ 'from' => "now-1d/d", 'to' => "now/d", 'key' => "day" ],
                            [ 'from' => "now-1w/d", 'to' => "now-1w+1d/d", 'key' => "week" ],
                            [ 'from' => "now-1M/d", 'to' => "now-1M+1d/d", 'key' => "month" ],
                            [ 'from' => "now-3M/d", 'to' => "now-3M+1d/d", 'key' => "trimester" ],
                            [ 'from' => "now-6M/d", 'to' => "now-6M+1d/d", 'key' => "semester" ],
                        ],
                    ],
                    'aggs' => [
                        'anomalies' => [
                            'aggs' => [
                                'analyzer' => [
                                    'terms' => [ 'field' => "anomalies.analyzer", ],
                                    'aggs' => [
                                        'count' => [
                                            'sum' => [ 'field' => "anomalies.objectCount", ],
                                        ],
                                    ],
                                ],
                            ],
                            'nested' => [ 'path' => "anomalies", ],
                        ],
                    ]
                ],
            ],
        ];
        $frequencyCode = strtoupper($frequency[0]);

        $search = new Search($this->client);
        $search->addIndex('statistics-*');

        // 1. national level users
        $recipients = $this->userRepository->getNotificationRecipients('national', true, $frequencyCode);

        if ($recipients) {
            $search->setQuery($this->getQuery($trendQuery, 'national', null));
            $report = $this->getReport($search->search()->getAggregations()['horizon']['buckets']);
            $notification = new AnomalyReportNotification($report, $frequency);
            $relevantRecipients = array_map(function ($user) {
                return $user->toRecipient();
            }, $recipients);
            if ($relevantRecipients) {
                $this->notifier->send($notification, ...$relevantRecipients);
            }
        }

        // 2. producers
        $recipients = $this->userRepository->getNotificationRecipients('producer', true, $frequencyCode);

        if ($recipients) {
            $producers = array_map(function ($user) {
                return $user->getHasOrganizationIdentifier();
            }, $recipients);
            $producers = array_unique($producers);
            foreach ($producers as $producer) {
                $search->setQuery($this->getQuery($trendQuery, 'producer', $producer));
                $report = $this->getReport($search->search()->getAggregations()['horizon']['buckets']);
                $notification = new AnomalyReportNotification($report, $frequency);
                $relevantRecipients = array_filter($recipients, function ($user) use ($producer) {
                    return $user->getHasOrganizationIdentifier() == $producer;
                });
                $relevantRecipients = array_map(function ($user) {
                    return $user->toRecipient();
                }, $relevantRecipients);
                if ($relevantRecipients) {
                    $this->notifier->send($notification, ...$relevantRecipients);
                }
            }
        }

        // 3. creators
        $recipients = $this->userRepository->getNotificationRecipients('creator', true, $frequencyCode);

        if ($recipients) {
            $creators = array_map(function ($user) {
                return !empty($user->getHasBeenCreatedBy()) ? $user->getHasBeenCreatedBy()[0] : null;
            }, $recipients);
            $creators = array_filter(array_unique($creators));
            foreach ($creators as $creator) {
                $search->setQuery($this->getQuery($trendQuery, 'creator', $creator));
                $report = $this->getReport($search->search()->getAggregations()['horizon']['buckets']);
                $notification = new AnomalyReportNotification($report, $frequency);
                $relevantRecipients = array_filter($recipients, function ($user) use ($creator) {
                     return $user->getHasBeenCreatedBy() == [$creator];
                });
                $relevantRecipients = array_map(function ($user) {
                    return $user->toRecipient();
                }, $relevantRecipients);
                if ($relevantRecipients) {
                    $this->notifier->send($notification, ...$relevantRecipients);
                }
            }
        }
    }

    private function sendThresholdNotifications()
    {
        $date = new DateTime('yesterday');
        $index = 'statistics-' . $date->format('Y.m');
        if ($date->format('j') == 1) {
            $index .= ',statistics-' . $date->sub(new DateInterval('1 day'))->format('Y.m');
        }
        $search = new Search($this->client);
        $search->addIndex($index);
        $thresholdQuery = [
            'size' => 0,
            'query' => [
                'bool' => [
                    'filter' => [
                        ['range' => [ 'date' => [ 'gte' => 'now-1w' ] ]]
                    ],
                ],
            ],
            'aggs' => [
                'horizon' => [
                    'date_range' => [
                        'field' => "date",
                        'format' => "yyyy-MM-dd",
                        'ranges' => [
                            [ 'from' => "now-1d/d", 'to' => "now/d", 'key' => "day-1" ],
                            [ 'from' => "now/d", 'to' => "now+1d/d", 'key' => "day" ],
                        ],
                    ],
                    'aggs' => [
                        'anomalies' => [
                            'aggs' => [
                                'analyzer' => [
                                    'terms' => [ 'field' => "anomalies.analyzer", ],
                                    'aggs' => [
                                        'count' => [
                                            'sum' => [ 'field' => "anomalies.objectCount", ],
                                        ],
                                    ],
                                ],
                            ],
                            'nested' => [ 'path' => "anomalies", ],
                        ],
                    ]
                ],
            ],
        ];

        // 1. national level users
        $recipients = $this->userRepository->getNotificationRecipients('national', false, null);

        if ($recipients) {
            $search->setQuery($this->getQuery($thresholdQuery, 'national', null));
            $results = $search->search()->getAggregations()['horizon']['buckets'];
            $this->notifyThresholdAlert($results, $recipients);
        }

        // 2. producers
        $recipients = $this->userRepository->getNotificationRecipients('producer', false, null);

        if ($recipients) {
            $producers = array_map(function ($user) {
                return $user->getHasOrganizationIdentifier();
            }, $recipients);
            $producers = array_unique($producers);
            foreach ($producers as $producer) {
                $search->setQuery($this->getQuery($thresholdQuery, 'producer', $producer));
                $results = $search->search()->getAggregations()['horizon']['buckets'];
                $this->notifyThresholdAlert($results, $recipients);
            }
        }

        // 3. creators
        $recipients = $this->userRepository->getNotificationRecipients('creator', false, null);

        if ($recipients) {
            $creators = array_map(function ($user) {
                return $user->getHasBeenCreatedBy()[0];
            }, $recipients);
            $creators = array_unique($creators);
            foreach ($creators as $creator) {
                $search->setQuery($this->getQuery($thresholdQuery, 'creator', $creator));
                $results = $search->search()->getAggregations()['horizon']['buckets'];
                $this->notifyThresholdAlert($results, $recipients);
            }
        }
    }

    /**
     * @param array $query
     * @param string $userLevel
     * @param int|string|null $value
     * @return Query
     */
    private function getQuery(array $query, string $userLevel, $value): Query
    {
        $must = [$query['query']];
        $must_not = [];

        if ($userLevel == 'creator') {
            $must[] = ["terms" => ["hasBeenCreatedBy" => [$value] ]];
        } elseif ($userLevel == 'producer') {
            $must[] = ["term" => ["hasOrganizationIdentifier" => ["value" => $value]]];
            $must_not[] = ["exists" => ["field" => "hasBeenCreatedBy"]];
        } else {
            $must_not[] = ["exists" => ["field" => "hasOrganizationIdentifier"]];
        }

        $query['query'] = ["bool" => ["must" => $must, "must_not" => $must_not]];

        return new Query($query);
    }

    /**
     * @param $results
     * @return array
     */
    private function getReport($results): array
    {
        $report = [];

        // get date and counts by anomaly type
        foreach ($results as $result) {
            $horizon = $result['key'];
            $day = $result['from_as_string'];
            $buckets = $result['anomalies']['analyzer']['buckets'];
            foreach ($buckets as $bucketKey => $bucket) {
                $report[$horizon]['date'] = $day;
                $report[$horizon][$bucket['key']]['count'] = $bucket['count']['value'];
            }
        }

        $todayCount = [];
        foreach ($report['today'] as $bucketKey => $bucket) {
            if ($bucketKey != 'date') {
                $todayCount[$bucketKey] = $bucket['count'];
            }
        }

        // get count variations by anomaly type
        foreach ($report as $horizon => $data) {
            foreach ($data as $bucketKey => $datum) {
                if (isset($datum['count'])) {
                    $report[$horizon][$bucketKey]['evolution'] = $datum['count'] > 0
                        ? ( ($todayCount[$bucketKey] ?? 0)  - $datum['count'] ) / $datum['count'] * 100.
                        : null;
                }
            }
        }

        return $report;
    }

    /**
     * @param $results
     * @param array|null $recipients
     * @return void
     */
    private function notifyThresholdAlert($results, ?array $recipients)
    {
        $counts = $this->getCounts($results);
        $relevantRecipients = $this->getRelevantRecipients($recipients, $counts);
        $report = $this->addThresholdsToCounts($relevantRecipients, $counts);
        $notification = new AnomalyReportNotification($report, 'threshold');
        if ($relevantRecipients) {
            $relevantRecipients = array_map(function ($user) {
                return $user->toRecipient();
            }, $relevantRecipients);
            $this->notifier->send($notification, ...$relevantRecipients);
        }
    }

    /**
     * @param $results
     * @return array
     */
    private function getCounts($results): array
    {
        $counts = [];
        foreach ($results as $result) {
            $day = $result['key'];
            $buckets = $result['anomalies']['analyzer']['buckets'];
            foreach ($buckets as $bucket) {
                $counts[$bucket['key']][$day] = $bucket['count']['value'] ?? 0.0;
            }
        }
        foreach ($counts as $key => $count) {
            $counts[$key]['evolution'] = $count['day-1'] == 0.0
                ? null
                : round(($count['day'] - $count['day-1']) / $count['day-1'] * 100.);
        }
        return $counts;
    }

    /**
     * @param array|null $recipients
     * @param array $counts
     * @return array
     */
    private function getRelevantRecipients(?array $recipients, array $counts): array
    {
        return array_filter($recipients, function ($user) use ($counts) {
            $thresholdExceeded = false;
            foreach ($counts as $key => $count) {
                $threshold = $user->getThresholdNotifications()[lcfirst(str_replace("Analyzer", "", $key))] ?? null;
                if ($threshold && $count['evolution'] >= $threshold) {
                    $thresholdExceeded = true;
                    break;
                }
            }
            return $thresholdExceeded;
        });
    }

    /**
     * @param array $relevantRecipients
     * @param array $counts
     * @return array
     */
    private function addThresholdsToCounts(array $relevantRecipients, array $counts): array
    {
        foreach ($relevantRecipients as $recipient) {
            foreach ($counts as $key => $count) {
                $threshold = $recipient->getThresholdNotifications();
                $counts[$key]['threshold'] = $threshold[lcfirst(str_replace("Analyzer", "", $key))] ?? null;
            }
        }
        return $counts;
    }
}
