<?php

namespace App\Command;

use App\Entity\OrganizationType;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserCommand extends Command
{
    protected EntityManagerInterface $em;
    protected UserPasswordHasherInterface $passwordEncoder;

    public function __construct(EntityManagerInterface $em, UserPasswordHasherInterface $passwordEncoder)
    {
        parent::__construct();
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('app:user:create')
            ->setDescription('Création d\'un utilisateur');
    }

    /**
     * Execute
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // email
        $helper = $this->getHelper('question');
        $question = new Question('Email : ');
        $question->setValidator(function ($email) {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new \RuntimeException('Adresse email invalide');
            }

            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
            if ($user != null) {
                throw new \RuntimeException('Cet utilisateur existe déjà');
            }
            return $email;
        });
        $email = $helper->ask($input, $output, $question);

        // nom
        $helper = $this->getHelper('question');
        $question = new Question('Nom : ');
        $lastName = $helper->ask($input, $output, $question);

        // nom
        $helper = $this->getHelper('question');
        $question = new Question('Prénom : ');
        $firstName = $helper->ask($input, $output, $question);

        // organization
        $helper = $this->getHelper('question');
        $question = new Question('Organisation : ', 'Non renseignée');
        $organization = $helper->ask($input, $output, $question);

        // organization_type
        $helper = $this->getHelper('question');
        $repository = $this->em->getRepository(OrganizationType::class);
        $question = new ChoiceQuestion("Type d'organanisation", $repository->findAll(), 0);
        $organizationType = $helper->ask($input, $output, $question);

        // role
        $helper = $this->getHelper('question');
        $question = new Question('Role (ROLE_USER) : ', 'ROLE_USER');
        $question->setValidator(function ($role) {
            if (!in_array($role, ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_MANAGER'])) {
                throw new \RuntimeException('Role ' . $role . ' does not exists.');
            }
            return $role;
        });
        $role = $helper->ask($input, $output, $question);

        // password
        $helper = $this->getHelper('question');
        $question = new Question('Veuillez saisir un mot de passe : ');
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        $password = $helper->ask($input, $output, $question);

        $user = new User();
        $user->setEmail($email);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setOrganization($organization);
        $user->setOrganizationType($organizationType);
        $user->setRole($role);
        $user->setEnabled(true);
        $hashedPassword = $this->passwordEncoder->hashPassword($user, $password);
        $user->setPassword($hashedPassword);
        $this->em->persist($user);
        $this->em->flush();

        $output->writeln('Utilisateur correctement généré !');

        return Command::SUCCESS;
    }
}
