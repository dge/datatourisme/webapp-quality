<?php

namespace App\Command;

use App\Entity\Creator;
use App\Entity\Producer;
use App\Service\SparqlClient;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizeCreatorCommand extends Command
{
    protected EntityManagerInterface $em;
    protected SparqlClient $sparql;

    public function __construct(EntityManagerInterface $em, SparqlClient $sparql)
    {
        parent::__construct();
        $this->em = $em;
        $this->sparql = $sparql;
    }

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('app:sync:creator')
            ->setDescription('Synchronisation des créateurs de données à partir du SPARQL Endpoint');
    }

    /**
     * Execute
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $producerRepository = $this->em->getRepository(Producer::class);
        $creatorRepository = $this->em->getRepository(Creator::class);

        $progressBar = new ProgressBar($output);
        $progressBar->start();

        // prepare the progress bar
        $countQuery = 'SELECT (COUNT(DISTINCT ?uri) as ?count) WHERE { ?p <https://www.datatourisme.gouv.fr/ontology/core#hasBeenCreatedBy> ?uri }';
        $count = $this->sparql->selectOne($countQuery);
        $progressBar->setMaxSteps((int) $count['count']);

        // prepare the query
        $response = $this->sparql->select("
            SELECT DISTINCT ?uri ?legalName ?orgIdentifier WHERE {
                ?p <https://www.datatourisme.gouv.fr/ontology/core#hasBeenCreatedBy> ?uri.
                ?p <https://www.datatourisme.gouv.fr/resource/metadata#hasOrganizationIdentifier> ?orgIdentifier.
                ?uri <http://schema.org/legalName> ?legalName
            }
        ");

        $date = new DateTime();

        foreach ($response as $row) {
            $orgIdentifier = (int) $row['orgIdentifier'];
            $producer = $producerRepository->find($orgIdentifier);
            if ($producer) {
                $creator = $creatorRepository->findOneBy(['uri' => $row['uri'], 'producer' => $producer]);
                if (!$creator) {
                    $creator = new Creator();
                    $creator->setUri($row['uri']);
                    $creator->setProducer($producer);
                    $this->em->persist($creator);
                }
                $creator->setLegalName($row['legalName']);
                $creator->setUpdated($date);
            }

            $progressBar->advance();
        }

        // flush
        $this->em->flush();

        // delete untouched creators
        $qb = $this->em->createQueryBuilder();
        $qb->delete(Creator::class, 'c')
            ->where("c.updated < :date")
            ->setParameter("date", $date)
            ->getQuery()->execute();

        $progressBar->finish();

        return Command::SUCCESS;
    }
}
