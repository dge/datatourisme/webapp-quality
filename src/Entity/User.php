<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\UserRepository;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Component\Notifier\Recipient\RecipientInterface;
use Symfony\Component\Validator\GroupSequenceProviderInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\GroupSequenceProvider()
 * @UniqueEntity(
 *     fields="email",
 *     message="Il existe déjà un utilisateur avec cette adresse mail"
 * )

 */
class User implements UserInterface, PasswordAuthenticatedUserInterface, GroupSequenceProviderInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $role = "ROLE_USER";

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    private $plainPassword;

    /**
     * @var bool
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled = false;

    /**
     * @var bool
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked = false;

    /**
     * @var bool
     * @ORM\Column(name="external", type="boolean")
     */
    private $external = false;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    protected $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(max=128)
     */
    protected $firstName;

    /**
     * @var string
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Assert\Length(max=16)
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max=64)
     */
    protected $organization;

    /**
     * @var OrganizationType
     * @ORM\ManyToOne(targetEntity="OrganizationType", inversedBy="organizations")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\NotBlank()
     */
    protected $organizationType;

    /**
     * @var array|null
     * @ORM\Column(type="simple_array", nullable=true)
     */
    protected $periodicNotifications;

    /**
     * @var array|null
     * @ORM\Column(type="json", nullable=true)
     */
    protected $thresholdNotifications;

    /**
     * @var Producer
     * @ORM\ManyToOne(targetEntity="Producer", inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\NotBlank(groups = {"ROLE_MANAGER", "ROLE_USER"})
     */
    private $producer;

    /**
     * @var array
     * @ORM\Column(type="json", nullable=true)
     */
    private $hasBeenCreatedBy;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $createdBy;

    /**
     * @var DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var DateTime $lastLogin
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    public function getFullName(): string
    {
        return $this->firstName . ' ' . mb_strtoupper($this->lastName, 'UTF-8');
    }

    public function getReverseFullName(): string
    {
        return mb_strtoupper($this->lastName, 'UTF-8') . ' ' . $this->firstName;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     */
    public function setLocked(bool $locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return bool
     */
    public function isExternal(): bool
    {
        return $this->external;
    }

    /**
     * @param bool $external
     */
    public function setExternal(bool $external)
    {
        $this->external = $external;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role ?: 'ROLE_USER';
    }

    /**
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        return [$this->role];
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     */
    public function setPlainPassword(?string $plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * @return User|null
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * Get $created
     *
     * @return  DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * Set $created
     *
     * @param DateTime $created
     * @return  self
     */
    public function setCreated(DateTime $created): User
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get $updated
     *
     * @return  DateTime
     */
    public function getUpdated(): Datetime
    {
        return $this->updated;
    }

    /**
     * Set $updated
     *
     * @param  DateTime  $updated  $updated
     *
     * @return  self
     */
    public function setUpdated(DateTime $updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get $updated
     *
     * @return  DateTime
     */
    public function getLastLogin(): ?Datetime
    {
        return $this->lastLogin;
    }

    /**
     * Set $updated
     *
     * @param DateTime $lastLogin
     * @return  self
     */
    public function setLastLogin(DateTime $lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get the value of phone
     *
     * @return  string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set the value of phone
     *
     * @param  string  $phone
     *
     * @return  self
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of organizationName
     *
     * @return  string
     */
    public function getOrganization(): string
    {
        return $this->organization;
    }

    /**
     * Set the value of organizationName
     *
     * @param string $organization
     * @return  self
     */
    public function setOrganization(string $organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get the value of organizationName
     *
     * @return OrganizationType|null
     */
    public function getOrganizationType(): ?OrganizationType
    {
        return $this->organizationType;
    }

    /**
     * Set the value of organizationName
     *
     * @param OrganizationType $organizationType
     * @return  self
     */
    public function setOrganizationType(OrganizationType $organizationType)
    {
        $this->organizationType = $organizationType;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getPeriodicNotifications(): ?array
    {
        return $this->periodicNotifications;
    }

    /**
     * @param array|null $periodicNotifications
     */
    public function setPeriodicNotifications(?array $periodicNotifications): void
    {
        $this->periodicNotifications = $periodicNotifications;
    }

    /**
     * @return array|null
     */
    public function getThresholdNotifications(): ?array
    {
        return $this->thresholdNotifications;
    }

    /**
     * @param array|null $thresholdNotifications
     */
    public function setThresholdNotifications(?array $thresholdNotifications): void
    {
        $this->thresholdNotifications = $thresholdNotifications;
    }

    /**
     * Get the value of hasOrganizationIdentifier
     *
     * @return  Producer
     */
    public function getProducer(): ?Producer
    {
        return $this->producer;
    }

    /**
     * Set the value of hasOrganizationIdentifier
     *
     * @param Producer|null $producer
     * @return  self
     */
    public function setProducer(?Producer $producer)
    {
        $this->producer = $producer;

        return $this;
    }

    /**
     * Get the value of hasOrganizationIdentifier
     *
     * @return  int
     */
    public function getHasOrganizationIdentifier(): ?int
    {
        return $this->producer ? $this->producer->getId() : null;
    }

    /**
     * Get the value of hasBeenCreatedBy
     *
     * @return  array
     */
    public function getHasBeenCreatedBy(): ?array
    {
        return $this->hasBeenCreatedBy;
    }

    /**
     * Set the value of hasBeenCreatedBy
     *
     * @param array|null $hasBeenCreatedBy
     *
     * @return  self
     */
    public function setHasBeenCreatedBy(?array $hasBeenCreatedBy)
    {
        $this->hasBeenCreatedBy = !empty($hasBeenCreatedBy) ?: null;

        return $this;
    }

    /**
     * @see GroupSequenceProviderInterface
     */
    public function getGroupSequence()
    {
        return ['User', $this->getRole()];
    }

    /**
     * @return array
     */
    public function getAccountStatus()
    {
        if ($this->locked) {
            return [
                'class' => 'danger',
                'label' => 'Bloqué',
            ];
        }

        if (!$this->enabled) {
            return [
                'class' => 'default',
                'label' => 'En attente',
            ];
        }

        return [
            'class' => 'success',
            'label' => 'Validé',
        ];
    }

    /**
     * @return RecipientInterface
     */
    public function toRecipient(): RecipientInterface
    {
        return new Recipient($this->getEmail(), $this->getPhone() ?: '');
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFullName();
    }
}
