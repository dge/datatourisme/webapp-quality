<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Pagination;

use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Component\Form\FormFactoryInterface;

class FilterablePaginatedListFactory
{
    protected $formFactory;
    protected $paginator;
    protected $queryUpdater;

    /**
     * FilterablePaginatedListFactory constructor.
     *
     * @param FormFactoryInterface          $formFactory
     * @param PaginatorInterface            $paginator
     * @param FilterBuilderUpdaterInterface $queryUpdater
     */
    public function __construct(FormFactoryInterface $formFactory, PaginatorInterface $paginator, FilterBuilderUpdaterInterface $queryUpdater)
    {
        $this->formFactory = $formFactory;
        $this->paginator = $paginator;
        $this->queryUpdater = $queryUpdater;
    }

    /**
     * @param   QueryBuilder      $queryBuilder
     * @param   string            $formClass
     * @param   null              $data
     * @param   array             $options
     *
     * @return  FilterablePaginatedList
     */
    public function getList(QueryBuilder $queryBuilder, string $formClass, $data = null, array $options = []): FilterablePaginatedList
    {
        $form = $this->formFactory->create($formClass, $data, $options);

        return new FilterablePaginatedList($queryBuilder, $form, $this->paginator, $this->queryUpdater);
    }
}
