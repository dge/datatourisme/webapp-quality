<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Notification;

use App\Entity\User;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

class InvitationNotification extends Notification implements EmailNotificationInterface
{
    private $invitationIssuer;
    private $user;
    private $token;

    public function __construct(User $invitationIssuer, User $user, string $token)
    {
        $this->invitationIssuer = $invitationIssuer;
        $this->user = $user;
        $this->token = $token;
        parent::__construct('Invitation');
    }

    public function getChannels(RecipientInterface $recipient): array
    {
        $this->importance('DATAtourisme');
        return ['email'];
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient, $transport);
        $message->getMessage()
            ->htmlTemplate('email/account/invite.html.twig')
            ->context([
                'invitationIssuer' => $this->invitationIssuer,
                'user' => $this->user,
                'token' => $this->token,
            ])
        ;

        return $message;
    }
}
