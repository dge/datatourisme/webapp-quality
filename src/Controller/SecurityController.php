<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\InvitationType;
use App\Form\Type\StrongPasswordType;
use App\Notification\RegisteredNotification;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\FormLoginAuthenticator;
use Symfony\Component\Validator\Constraints\Email;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

/**
 * @Route(name="security.")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/logout", name="logout", methods={"GET"})
     */
    public function logout(): void
    {
        // controller can be blank: it will never be called!
    }

    /**
     * @Route("/reset-password", name="reset_password")
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param MailerInterface $mailer
     * @return Response
     * @throws NonUniqueResultException
     * @throws TransportExceptionInterface
     */
    public function resetPassword(
        Request $request,
        UserRepository $repository,
        UserPasswordHasherInterface $userPasswordHasher,
        MailerInterface $mailer
    ): Response {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'attr' => ['placeholder' => 'Email'],
                'constraints' => [new Email()]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $user = $repository->getUserByEmail($email, true);

            if ($user && !$user->isExternal()) {
                // generate new token
                $now = new \DateTime();
                $bcrypt = $userPasswordHasher->hashPassword($user, $now->getTimestamp() . $user->getPassword());

                // send link
                $email = (new TemplatedEmail())
                    ->to(new Address($user->getEmail()))
                    ->subject('Réinitialisation de votre mot de passe')
                    ->htmlTemplate('email/account/reset_password.html.twig')
                    ->context([
                        'account' => $user,
                        'bcrypt' => $bcrypt,
                        'expiration' => $this->getParameter('reset_password_expiration'),
                        'timestamp' => $now->getTimestamp()
                    ]);

                $mailer->send($email);
            }

            $this->addFlash('success', 'Si ce compte existe, un email vous a été envoyé pour réinitialiser votre mot de passe');

            return $this->redirectToRoute('security.login');
        }

        return $this->render('security/reset_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Check reset link and manage user update password.
     *
     * @param Request $request
     * @param User $user
     * @param string $timestamp
     * @param string $bcrypt
     * @param PasswordHasherFactoryInterface $passwordHasherFactory
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @return Response
     * @Route("/reset-password/{email}/{timestamp}/{bcrypt}", requirements={"bcrypt" = ".+"}, name="reset_password.process")
     * @ParamConverter("user", options={"mapping": {"email" = "email"}})
     *
     */
    public function resetPasswordProcess(
        Request $request,
        User $user,
        $timestamp,
        $bcrypt,
        PasswordHasherFactoryInterface $passwordHasherFactory,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface $em
    ): Response {
        // compute number of seconds since link has been sent
        $elapsed = time() - $timestamp;

        // token is not expired
        if ($elapsed < $this->getParameter('reset_password_expiration')) {
            // check bcrypt validity with user salt and password
            $bcryptIsValid = $passwordHasherFactory->getPasswordHasher($user)->verify($bcrypt, $timestamp . $user->getPassword());
            if ($bcryptIsValid) {
                $form = $this->createFormBuilder()
                    ->add('password', StrongPasswordType::class, ['user' => $user])
                    ->getForm();

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $password = $form->get('password')->getData();
                    $hashed = $userPasswordHasher->hashPassword($user, $password);
                    $user->setPassword($hashed);

                    // $user->updateTimestamps();
                    // $user->setCredentialsNonExpired(true);

                    // if user is new, its account is enabled
                    // if ($user->getLastLogin() === null && !$user->isEnabled()) {
                    //     $user->setEnabled(true);
                    //     // dispatch notification
                    //     $this->get('notifier')->dispatch(UserCreateType::class, $user);
                    // }

                    $em->flush();
                    $this->addFlash('success', 'Votre mot de passe a bien été mis à jour.');

                    return $this->redirectToRoute('security.login');
                }

                return $this->render('security/reset_password_process.html.twig', [
                    'form' => $form->createView(),
                    'user' => $user,
                ]);
            }
        }

        $this->addFlash('warning', 'Le lien fourni n\'est plus valide. Vous devez faire une nouvelle demande de réinitialisation de mot de passe.');
        return $this->redirectToRoute('security.reset_password');
    }

    /**
     * Complete invited user registration.
     *
     * @param Request $request
     * @param User $user
     * @param string $bcrypt
     * @param NotifierInterface $notifier
     * @param UserRepository $userRepository
     * @param PasswordHasherFactoryInterface $passwordHasherFactory
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param UserAuthenticatorInterface $userAuthenticator
     * @param FormLoginAuthenticator $formLoginAuthenticator
     * @return Response
     * @Route("/invite/{email}/{bcrypt}", requirements={"bcrypt" = ".+"}, name="invite.process")
     * @ParamConverter("user", options={"mapping": {"email" = "email"}})
     */
    public function inviteProcess(
        Request $request,
        User $user,
        string $bcrypt,
        NotifierInterface $notifier,
        PasswordHasherFactoryInterface $passwordHasherFactory,
        UserPasswordHasherInterface $userPasswordHasher,
        UserAuthenticatorInterface $userAuthenticator,
        FormLoginAuthenticator $formLoginAuthenticator,
        EntityManagerInterface $em
    ): Response {
        // check bcrypt validity with user salt and password
        $bcryptIsValid = $passwordHasherFactory->getPasswordHasher($user)->verify($bcrypt, $user->getPassword());
        if ($bcryptIsValid) {
            $userForm = $this->createForm(InvitationType::class, $user);
            $userForm->handleRequest($request);
            if ($userForm->isSubmitted() && $userForm->isValid()) {
                // persist confirmed user information
                $password = $userForm->get('password')->getData();
                $hashed = $userPasswordHasher->hashPassword($user, $password);
                $user->setPassword($hashed);
                $user->setEnabled(true);
                $em->flush();

                // send confirmation email
                $invitationIssuer = $user->getCreatedBy();
                if ($invitationIssuer) {
                    $notification = new RegisteredNotification($user, $invitationIssuer);
                    $notifier->send($notification, $invitationIssuer->toRecipient());
                }

                $this->addFlash('success', 'Vous êtes maintenant connecté.');
                return $userAuthenticator->authenticateUser($user, $formLoginAuthenticator, $request);
            }

            return $this->render('security/invite_process.html.twig', [
                'form' => $userForm->createView(),
                'user' => $user,
            ]);
        }

        return $this->redirectToRoute('home');
    }
}
