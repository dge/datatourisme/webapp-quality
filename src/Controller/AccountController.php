<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\Type\AccountParamsType;
use App\Form\Type\AccountType;
use App\Form\Type\UpdatePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account")
 * @IsGranted("ROLE_USER")
 */
class AccountController extends AbstractController
{
    /**
     * @Route("", name="account.user")
     */
    public function user(Request $request, EntityManagerInterface $em): Response
    {
        $userForm = $this->createForm(AccountType::class, $this->getUser());

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Votre compte a bien été mis à jour.');

            return $this->redirectToRoute('account.user');
        }

        return $this->render('account/user.html.twig', [
            'form' => $userForm->createView(),
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/params", name="account.params")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function params(Request $request, EntityManagerInterface $em): Response
    {
        $anomalyTypes = [
            "links" => "Anomalie de lien web",
            "media" => "Anomalie de média",
            "text" => "Anomalie de contenu",
            "geoCoordinates" => "Anomalie géospatiale",
            "consistency" => "Anomalie de cohérence",
        ];

        $anomalyValues = [];
        $thresholdValues = [];
        $currentThresholdNotifications = $this->getUser()->getThresholdNotifications() ?? [];
        foreach ($anomalyTypes as $anomalyType => $anomalyLabel) {
            if (in_array($anomalyType, array_keys($currentThresholdNotifications))) {
                $anomalyValues[$anomalyType] = true;
                $thresholdValues[$anomalyType] = $currentThresholdNotifications[$anomalyType];
            } else {
                $anomalyValues[$anomalyType] = false;
                $thresholdValues[$anomalyType] = null;
            }
        }

        $userForm = $this->createForm(AccountParamsType::class, $this->getUser(), [
            'anomalyTypes' => $anomalyTypes,
        ]);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $newThresholdNotifications = [];
            foreach ($anomalyTypes as $anomalyType => $anomalyLabel) {
                if ($userForm->get("anomaly_$anomalyType")->getData()) {
                    $newThresholdNotifications[$anomalyType] = $userForm->get("threshold_$anomalyType")->getData();
                }
            }
            /** @var User $user */
            $user = $userForm->getData();
            $user->setThresholdNotifications($newThresholdNotifications);
            $em->flush();
            $this->addFlash('success', 'Votre compte a bien été mis à jour.');

            return $this->redirectToRoute('account.params');
        }

        return $this->render('account/params.html.twig', [
            'form' => $userForm->createView(),
            'user' => $this->getUser(),
            'anomalyTypes' => $anomalyTypes,
            'anomalyValues' => $anomalyValues,
            'thresholdValues' => $thresholdValues,
        ]);
    }

    /**
     * @Route("/administrate", name="account.administrate")
     * @Security("!is_granted('IS_IMPERSONATOR')")
     */
    public function administrate(): Response
    {
        return $this->render('account/administrate.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/administrate/password", name="account.administrate.password")
     * @Security("!is_granted('IS_IMPERSONATOR') and is_granted('user.modify_password')")
     */
    public function administratePassword(Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(UpdatePasswordType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $user->updateTimestamps();
            $em->flush();
            $this->addFlash('success', 'Votre mot de passe a bien été mis à jour.');

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('account/administrate/password.html.twig', [
            'form' => $form->createView(),
            'user' => $this->getUser(),
        ]);
    }
}
