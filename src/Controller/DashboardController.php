<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\User;
use App\Repository\ProducerRepository;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/dashboard", name="dashboard.")
*/
class DashboardController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(ProducerRepository $producerRepository): Response
    {
        $all = $producerRepository->findAll();
        $producers = new stdClass();
        foreach ($all as $p) {
            $producers->{$p->getId()} = $p->getName();
        }

        return $this->render('dashboard/index.html.twig', [
            "producers" => $producers
        ]);
    }

    /**
     * @Route("/data/map", name="data.map")
     */
    public function dataMap(Security $security): Response
    {
        /** @var User */
        $user = $security->getUser();
        $path = $this->getParameter('kernel.project_dir') . '/var/files/map/';

        $files = [];
        if ($user->getProducer()) {
            if ($user->getHasBeenCreatedBy() && count($user->getHasBeenCreatedBy())) {
                $files = array_map(function ($uri) {
                    return "creators/" . md5($uri) . ".json";
                }, $user->getHasBeenCreatedBy());
            } else {
                $files = ["producers/" . $user->getProducer()->getId() . ".json"];
            }
        } else {
            $files = ["data.json"];
        }

        if (count($files) == 1) {
            $content = file_get_contents($path . $files[0]);
        } else {
            $contents = [];
            foreach ($files as $i => $file) {
                $contents[] = substr(file_get_contents($path . $files[0]), 1, -1);
            }
            $content = "[" . join(",", $contents) . "]";
        }

        return new JsonResponse($content, 200, [], true);
    }
}
