<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\News;
use App\Form\Type\NewsType;
use App\Notification\NewsNotification;
use App\Repository\NewsRepository;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/news", name="news")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("/", name=".index")
     * @Route("/{month}", name=".index.month", defaults={"month": null}, requirements={"month": "\d{4}-\d{2}"})
     */
    public function index(NewsRepository $repository, string $month = null): Response
    {
        $months = $repository->getMonthYearDistribution();

        if (!empty($month)) {
            list($y, $m) = explode("-", $month);
            $news = $repository->findByYearMonth($y, $m);
        } else {
            $news = $repository->findBy([], ['date' => 'DESC', 'id' => 'DESC']);
        }

        return $this->render('news/index.html.twig', [
            'months' => $months,
            'active_month' => $month,
            'total' => array_sum($months),
            'news_list' => $news
        ]);
    }

    /**
     * @Route("/{id}", name=".detail", requirements={"id": "\d+"})
     */
    public function detail(News $news, Request $request): Response
    {
        return $this->render('news/detail.html.twig', [
            'news' => $news
        ]);
    }

    /**
     * @Route("/add", name=".add")
     * @IsGranted("news.add")
     */
    public function add(
        Request $request,
        FileUploader $fileUploader,
        NotifierInterface $notifier,
        UserRepository $repository,
        EntityManagerInterface $em
    ): Response {
        $news = new News();
        return $this->edit($news, $request, $fileUploader, $notifier, $repository, $em);
    }

    /**
     * @Route("/edit/{id}", name=".edit", requirements={"id": "\d+"})
     * @IsGranted("news.edit", subject="news")
     */
    public function edit(
        News $news,
        Request $request,
        FileUploader $fileUploader,
        NotifierInterface $notifier,
        UserRepository $repository,
        EntityManagerInterface $em
    ): Response {
        $new = empty($news->getId());
        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            if ($form->has('removeFile') && $form->get('removeFile')->getData()) {
                $fileUploader->remove("news", $news->getFilename());
                $news->setFilename("");
            }
            if ($form->has('file') && $form->get('file')->getData()) {
                $fileName = $fileUploader->upload("news", $form->get('file')->getData());
                $news->setFilename($fileName);
            }
            $em->persist($news);
            $em->flush();
            $this->addFlash('success', $new ? "L'actualité a été ajoutée." : "L'actualité a été modifiée.");
            if ($new) {
                $notification = new NewsNotification($news);
                $recipients = $repository->getRecipientsByRole('ROLE_USER');
                if ($recipients) {
                    $notifier->send($notification, ...$recipients);
                }
            }
            return $this->redirectToRoute('news.index');
        }

        return $this->render('news/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/download", name=".download", requirements={"id": "\d+"})
     */
    public function download(News $news)
    {
        if ($news->getFilename()) {
            $response = new BinaryFileResponse($this->getParameter('upload_directory') . '/news/' . $news->getFilename());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $news->getFileName());
            return $response;
        }
        throw $this->createNotFoundException();
    }

    /**
     * @Route("/remove/{id}", name=".remove", requirements={"id": "\d+"})
     * @IsGranted("news.remove", subject="news")
     */
    public function remove(News $news, Request $request, FileUploader $fileUploader, EntityManagerInterface $em): Response
    {
        if ($request->getMethod() === 'POST') {
            if ($news->getFilename()) {
                $fileUploader->remove("news", $news->getFilename());
            }
            $em->remove($news);
            $em->flush();

            $this->addFlash('success', "L'actualité a bien été supprimée.");

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('news.index'),
            ], 200);
        }

        return $this->render('news/remove.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * @Route("/api/last", name=".api.last")
     */
    public function apiLast(Request $request, NewsRepository $repository, SerializerInterface $serializer): Response
    {
        $news = $repository->findOneBy([], ['date' => 'DESC', 'id' => 'DESC']);
        $repr = $serializer->serialize($news, 'json');

        return JsonResponse::fromJsonString($repr);
    }
}
