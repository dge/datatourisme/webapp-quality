<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Twig;

use Moment\Moment;
use Moment\MomentLocale;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class MomentExtension.
 */
class MomentExtension extends AbstractExtension
{
    /**
     * @var string
     */
    protected $locale;

    /**
     * @var string
     */
    protected $timezone;

    /**
     * @var array
     */
    static $localeMap = [
        'ar' => 'ar_TN',
        'ca' => 'ca_ES',
        'cs' => 'cs_CZ',
        'da' => 'da_DK',
        'de' => 'de_DE',
        'en' => 'en_GB',
        //'en' => 'en_US',
        'es' => 'es_ES',
        'fr' => 'fr_FR',
        'hu' => 'hu_HU',
        'id' => 'id_ID',
        'it' => 'it_IT',
        'ja' => 'ja_JP',
        'nl' => 'nl_NL',
        'oc' => 'oc_LNC',
        'pl' => 'pl_PL',
        'pt' => 'pt_BR',
        'ro' => 'ro_RO',
        'ru' => 'ru_RU',
        'se' => 'se_SV',
        'th' => 'th_TH',
        'tr' => 'tr_TR',
        'uk' => 'uk_UA',
        'vi' => 'vi_VN',
        'zh' => 'zh_CN',
        //'zh' => 'zh_TW',
    ];

    /**
     * @param $locale
     * @return mixed
     */
    static function guessLocale($locale)
    {
        return isset(self::$localeMap[$locale]) ? self::$localeMap[$locale] : $locale;
    }

    /**
     * MomentExtension constructor.
     *
     * @param RequestStack $requestStack
     * @param string $timezone
     */
    public function __construct(RequestStack $requestStack, string $timezone = "Europe/Paris")
    {
        if ($requestStack->getCurrentRequest()) {
            $locale = $requestStack->getCurrentRequest()->getLocale();
            $this->locale = $this->guessLocale($locale);
        }
        $this->timezone = $timezone;
    }

    /**
     * Returns a list of filters.
     *
     * @return array
     */
    public function getFilters()
    {
        $filters = [
            new TwigFilter('moment', [$this, 'twigMoment']),
            new TwigFilter('moment_elapsed', [$this, 'twigMomentElapsed']),
        ];

        return $filters;
    }

    /**
     * Name of this extension.
     *
     * @return string
     */
    public function getName()
    {
        return 'moment';
    }

    /**
     * Moment a PHP date.
     *
     * @param \DateTime $date
     * @param string $locale
     *
     * @return Moment
     * @throws \Moment\MomentException
     */
    public function twigMoment($date, string $locale = null): Moment
    {
        $locale = $locale ? $this->guessLocale($locale) : $this->locale;
        $timezone = $this->timezone ?: 'UTC';

        if (!$date instanceof \DateTime) {
            $date = new \DateTime($date);
        }
        $date->setTimezone(new \DateTimeZone($timezone));
        Moment::setDefaultTimezone($timezone);

        if ($locale) {
            Moment::setLocale($locale);
        }

        return new Moment(date_format($date, 'Y-m-d H:i:s'));
    }

    /**
     * Diff two Moment date.
     *
     * @param \DateTime $date
     * @param \DateTime $dateEnd
     * @param string    $prefix
     * @param string    $suffix
     *
     * @return string
     */
    public function twigMomentElapsed($date, $dateEnd, $prefix = '', $suffix = '')
    {
        if (!$date instanceof \DateTime) {
            $date = new \DateTime($date);
        }
        if (!$dateEnd instanceof \DateTime) {
            $dateEnd = new \DateTime($dateEnd);
        }
        $formatElapsed = function ($count, $format) use (&$formatElapsed) {
            $format = (1 === $count || 's' === $format) ? $format : $format . $format;

            return MomentLocale::renderLocaleString(['relativeTime', $format], [$count]);
        };

        $dateDiff = $date->diff($dateEnd);
        if ($v = $dateDiff->y >= 1) {
            return $prefix . $formatElapsed($dateDiff->y, 'y') . $suffix;
        }
        if ($v = $dateDiff->m >= 1) {
            return $prefix . $formatElapsed($dateDiff->m, 'M') . $suffix;
        }
        if ($v = $dateDiff->d >= 1) {
            return $prefix . $formatElapsed($dateDiff->d, 'd') . $suffix;
        }
        if ($v = $dateDiff->h >= 1) {
            return $prefix . $formatElapsed($dateDiff->h, 'h') . $suffix;
        }
        if ($v = $dateDiff->i >= 1) {
            return $prefix . $formatElapsed($dateDiff->i, 'm') . $suffix;
        }

        return $prefix . $formatElapsed($dateDiff->s, 's') . $suffix;
    }
}
