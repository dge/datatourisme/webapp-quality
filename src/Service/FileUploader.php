<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: Apache-2.0
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private string $targetDirectory;
    private SluggerInterface $slugger;

    public function __construct($targetDirectory, SluggerInterface $slugger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }

    /**
     * @param string $folder
     * @param UploadedFile $file
     * @return string
     */
    public function upload(string $folder, UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        $file->move($this->getTargetDirectory() . '/' . $folder, $fileName);

        return $fileName;
    }

    /**
     * @param string $folder
     * @param $filename
     * @return void
     */
    public function remove(string $folder, $filename): bool
    {
        try {
            $file = new File($this->getTargetDirectory() . '/' . $folder . '/' . $filename);
            return unlink($file);
        } catch (FileNotFoundException $e) {
            return true;
        }
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
