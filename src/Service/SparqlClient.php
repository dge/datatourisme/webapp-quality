<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: Apache-2.0
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service;

use Generator;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\StreamableInterface;
use Symfony\Component\HttpClient\Response\StreamWrapper;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SparqlClient
{
    private HttpClientInterface $client;

    /**
    * Constructor
    */
    public function __construct(string $url, array $options = [])
    {
        // initialize client
        $this->client = HttpClient::createForBaseUri($url, $options);
    }

    /**
     * select
     */
    public function select(string $query): iterable
    {
        $response = $this->client->request('POST', '', [
            'buffer' => false,
            'headers' => ['Accept' => 'text/csv'],
            'body' => ['query' => $query]
        ]);

        $keys = null;
        $resource = StreamWrapper::createResource($response, $this->client);
        while (($data = fgetcsv($resource, 1000, ",")) !== false) {
            if (!$keys) {
                $keys = $data;
            } else {
                yield array_combine($keys, $data);
            }
        }
    }

    /**
     * selectOne
     */
    public function selectOne(string $query): array
    {
        foreach ($this->select($query) as $row) {
            return $row;
        }

        return null;
    }
}
