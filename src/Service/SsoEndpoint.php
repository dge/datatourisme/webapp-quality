<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: Apache-2.0
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Service;

use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Signature\Exception\ExpiredSignatureException;
use Symfony\Component\Security\Core\Signature\Exception\InvalidSignatureException;

class SsoEndpoint
{
    private $router;
    private string $endpoint;
    private string $secret;
    private string $redirectRoute;
    private SessionInterface $session;
    private int $expiration = 3000;

    /**
    * Constructor
    */
    public function __construct(
        string $endpoint,
        string $secret,
        string $redirectRoute,
        UrlGeneratorInterface $router,
        SessionInterface $session
    ) {
        $this->endpoint = $endpoint;
        $this->secret = $secret;
        $this->redirectRoute = $redirectRoute;
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * Undocumented function
     *
     * @return RedirectResponse
     */
    public function redirect(): RedirectResponse
    {
        $params = ['nonce' => $this->generateNonce()];
        $sso = base64_encode(http_build_query($params));
        $data = [
            'sso' => $sso,
            'sig' => $this->encode($sso, $this->secret),
            'redirect_url' => $this->getRedirectUrl()
        ];
        $qs = http_build_query($data);
        return new RedirectResponse($this->endpoint . '?' . $qs);
    }

    /**
     * Generate a new nonce
     *
     * @return void
     */
    public function generateNonce()
    {
        $this->session->start();
        return time() . "." . $this->encode(time() . $this->session->getId());
    }

    /**
     * Check a nonce validity
     *
     * @throws \InvalidSignatureException
     * @throws \ExpiredSignatureException
     */
    public function validateNonce(string $nonce)
    {
        list($timestamp, $sessionHash) = explode(".", $nonce);

        if (($timestamp + $this->expiration) < time()) {
            throw new ExpiredSignatureException('Nonce has expired.');
        }

        $this->session->start();
        if (!hash_equals($this->encode($timestamp . $this->session->getId()), $sessionHash)) {
            throw new InvalidSignatureException("Invalid nonce found");
        }
    }

    /**
     * @return mixed
     *
     * @throws \Exception
     */
    public function decode(string $sso, string $signature)
    {
        $this->validate($sso, $signature);
        $sso = urldecode($sso);
        $data = [];
        parse_str(base64_decode($sso), $data);

        if (!array_key_exists('nonce', $data)) {
            throw new \Exception('Not found nonce in payload');
        }

        $this->validateNonce($data['nonce']);

        return $data;
    }

    /**
     * @param $secret
     * @param $signature
     *
     * @throws \InvalidSignatureException
     */
    private function validate(string $sso, string $signature)
    {
        $sso = urldecode($sso);
        if (!hash_equals($this->encode($sso), $signature)) {
            throw new InvalidSignatureException('invalid Signature');
        }
    }

    /**
     * Return redirect URL
     *
     * @return void
     */
    private function getRedirectUrl()
    {
        return $this->router->generate($this->redirectRoute, [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * Encode a value
     */
    private function encode(string $value)
    {
        return hash_hmac('sha256', $value, $this->secret);
    }
}
