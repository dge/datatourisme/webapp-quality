# DATAtourisme : plateforme qualité

## Setup

### Ajouter un fichier `env.local`

```
ELASTICSEARCH_URL=http://my_elasticsearch_host:9200/ # <-- the trailing slash is important !
ELASTICSEARCH_USERNAME=xxxx
ELASTICSEARCH_PASSWORD=xxxx
```

### Lancer les services

```
docker-compose up -d
```

### Installer les dépendances

```
docker-compose exec app composer install
docker-compose exec app yarn install
docker-compose exec app yarn encore dev
```

### Créer et charger la base de données

```
./console doctrine:schema:update --force
./console doctrine:migration:migrate --no-interaction
```

### Créer un utilisateur super admin

Création d'un utilisateur `admin@localhost.dev`/`admin`
avec le rôle de super-administrateur :

```
./console doctrine:fixtures:load --append
```

### Créer un nouvel utilisateur

Pour créer de nouveaux utilisateurs :

```
./console app:user:create
```

## Services

* Application : http://localhost:8000
* Adminer : http://localhost:9000
* Mailer: http://localhost:8080

### Activer la carte du dashboard en mode dev

Pour lancer le traitement de récupération des données des POI sur la carte en mode dev :

```
./console app:cron:map
```

## Recettes

### Extensions VSCode

* junstyle.php-cs-fixer

### Corriger le code

```
php vendor/bin/php-cs-fixer fix src
```
